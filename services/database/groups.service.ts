import { ad, groups, invitation_states, requests, request_states } from "@prisma/client";
import { prisma, Service } from ".";
import {Ad, Group, Invitation, InviteResolution, RequestResolution, Request} from "../../interfaces";

/**
 * @returns Array of all existing groups packed in Promise object
 */
export const getAllGroups: Service =  async ():Promise<Group[]> => {
    try {
     return await prisma.groups.findMany({
         include:{
             game: true,
             members: true
         },
         orderBy:{
            id: 'asc'
        }
     })
    } catch (error) {
        console.log("Faild to fetch groups!")
        console.log(error)
        throw error
    }
    
}

/**
 * Returns group by ID
 * @param id group ID
 * @returns Group packed in Promise object
 */
export const getGroup: Service = async (id: number): Promise<Group> => {
    try {
        return (await prisma.groups.findOne({
          where: {
            id,
          },
          include: {
            game: true,
            owner: true,
            members: {
               include: {users: true}
            },
          },
        }))
    } catch (error) {
        console.log("Failed to fetch group "+id+"!")
        console.log(error)
        throw error
    }
}

/**
 * Deletes group by ID
 * @param id Group ID
 * @returns deleted Group instance packed in Promise object
 */
export const deleteGroup: Service = async (id: number): Promise<Group> => {
    try {
        return await prisma.groups.delete({
            where:{
                id
            },
            include:{
                owner: true,
                game: true
            }
        })
    } catch (error) {
        console.log("Failed to delete group "+id+"!")
        console.log(error)
        throw error
    }
}

/**
 * Updates group by ID
 * @param group Group instance
 * @returns updated Group instance packed in Promise object
 */
export const updateGroup: Service = async (group: Group): Promise<groups> => {
    try {
        return await prisma.groups.update({
        where:{
            id: group.id
        },
        data:{
            name: group.name,
            capacity: group.capacity,
            description: group.description,
        }
    })
    } catch (error) {
        console.log("Failed to delete group "+group.id+"!")
        console.log(error)
        throw error
    }
}


/**
 * @param group Group parameters to create new group instance in DB
 * @returns created instance
 * @async
 */
export const createGroup: Service = async (group: Group): Promise<groups> => {
    const { owner_id, ...rest } = group;

    try {
        const owner_id = group.owner_id
        delete group.owner_id
        return await prisma.groups.create({
            data:{
                ...group,
                owner:{
                    connect:{
                        id:owner_id
                    }
                },
                game:{
                    connect:{
                        id: group.game.id
                    }
                }
            },
        })
    } catch (error) {
      console.log("Failed to create group");
      console.log(error);
      throw error;
    }
}

/**
 * 
 * @param group_id ID of group we want to get invitations for
 * @returns Array of Invitation instances in "pending" state
 * @async
 */
export const getInvitationsForGroup: Service = async (group_id: number): Promise<Invitation[]> => {
    try {
        return await prisma.invitation.findMany({
            where:{
                group_id,
                state: invitation_states.Pending
            },
            orderBy:{
                group_id: 'asc',
                player_id: 'asc'
            }
        })
    } catch (error) {
        console.log("Failed to fetch invitation for group "+group_id+"!")
        console.log(error)
        throw error
    }
}

/**
 * @param inviteResolution (InviteResolution type) including ID of player, ID of group and accepted state (boolean)
 * @returns Invitation instance in "accepted" or "rejected" state
 * @async
 * @todo this should only be allowed to group owner
 */
export const resolveInvitation: Service = async (inviteResolution: InviteResolution): Promise<Invitation> => {
    let player_id = inviteResolution.player_id
    let group_id = inviteResolution.group_id
    let accepted = inviteResolution.accepted

    try {
        return await prisma.invitation.update({
            where:{
                group_id_player_id:{
                    player_id,
                    group_id
                }
            },
            data:{
                state: accepted ? invitation_states.Accepted : invitation_states.Rejected
            }
        })
    } catch (error) {
        console.log("Failed to update invitation of user "+player_id+" for group "+group_id+"!")
        console.log(error)
        throw error
    }
}

/**
 * Creates ad to join into passed group
 * @param adv Ad instance to create in db
 * @returns created instance
 * @async
 */
export const createAdForGroup: Service = async (adv: Ad): Promise<ad> => {
    try{
        return await prisma.ad.create({
          data:{
              ...adv,
              location: "" + adv.location.latitude + adv.location.latitude,
              groups: {
                  connect:{
                      id: adv.group.id
                  },
              },
          },
        })
    }catch(error){
        console.log("Failed to create ad for group: "+adv.group+"!")
        console.log(error)
        throw error
    }
}

/**
 * User (player_id) makes request to join group defined in ad_id
 * @param player_id 
 * @param ad_id 
 * @async
 */
export const makeRequestForAd: Service = async(player_id: number, ad_id: number): Promise<requests> =>{
    try{
        return await prisma.requests.create({
            data:{
                adv:{
                    connect:{
                        id: ad_id
                    }
                },
                player:{
                    connect:{
                        id: player_id
                    }
                },
                state: request_states.Pending
            }
        })
    }catch(error){
        console.log("Failed to make entrance request for group!")
        console.log(error)
        throw error
    }
}

/**
 * @param group_id ID of group we want to get requests for
 * @returns Array of unsolved requests for specified group
 * @async
 * @todo only group owner should be allowed to do this
 */
export const getUnsolvedRequestsForGroup: Service = async(group_id: number): Promise<Request[]> => {
    try {
        return await prisma.requests.findMany({
            where:{
                state: request_states.Pending
            },
            include:{
                player:true,
                adv:true
            },
            orderBy:{
                player_id: 'asc',
                ad_id: 'asc',
            }
        })
    } catch (error) {
        console.log("Failed to fetch request for group "+group_id+"!")
        console.log(error)
        throw error
    }
}

/**
 * @param requestResolution requestResolution instance
 * @returns Invitation instance in "accepted" or "rejected" state
 * @async
 * @todo this should only be allowed to group owner
 */
export const resolveRequest: Service = async(requestResolution: RequestResolution): Promise<requests> => {
    try {
        return await prisma.requests.update({
            where:{
                player_id_ad_id:{
                    player_id: requestResolution.player_id,
                    ad_id: requestResolution.ad_id
                }
            },
            data:{
                state: requestResolution.accepted ? request_states.Accepted : request_states.Rejected
            }
        })
    } catch (error) {
        console.log("Failed to request for ad: "+requestResolution.ad_id+" started by player: "+requestResolution.player_id+"!")
        console.log(error)
        throw error
    }
}


