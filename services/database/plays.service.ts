import { prisma } from "./index";

export const getPlays = async (userId: number) => {
    return prisma.plays.findMany({
        where: { player_id: userId },
        include: { users: { select: { username: true } }, game: true },
        orderBy: {game_id: 'desc'}
    });
};

export const addPlay = async (
    userId: number,
    gameId: number,
    offerToRent: boolean | undefined,
    price: number | undefined,
    count: number
) => {
    const existingPlay = await prisma.plays.count({
        where: { player_id: userId, game_id: gameId },
    });

    if (existingPlay) {
        throw new Error("Play already exists");
    }

    return prisma.plays.create({
        data: {
            game: { connect: { id: gameId } },
            users: { connect: { id: userId } },
            offer_to_rent: offerToRent,
            price,
            count
        },
    });
};

export const updatePlay = async (
    userId: number,
    gameId: number,
    offerToRent: boolean | undefined,
    price: number | undefined,
    count: number | undefined
) => {
    const play = await prisma.plays.count({
        where: { users: { id: userId }, game_id: gameId },
    });

    if (!play) {
        throw new Error("Play doens't exists");
    }

    return prisma.plays.update({
        where: { game_id_player_id: { game_id: gameId, player_id: userId } },
        data: {
            offer_to_rent: offerToRent,
            price,
            count
        },
    });
};

export const deletePlay = async (userId: number, gameId: number) => {
    return prisma.plays.delete({
        where: { game_id_player_id: { game_id: gameId, player_id: userId } },
    });
};
