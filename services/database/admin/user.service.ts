import { prisma } from "../index";

export const getAllUsers = () => {
  return prisma.users.findMany({ orderBy: { id: "desc" } });
};

export const getUser = (id: number) => {
  return prisma.users.findUnique({
    where: { id },
    include: {
      plays: { include: { game: true } },
      groups: true,
      company: true,
    },
  });
};

export const acceptUser = (id: number) => {
  return prisma.users.update({ where: { id }, data: { is_accepted: true } });
};

export const updateUser = (id: number, user: any) => {
  return prisma.users.update({ where: { id }, data: { ...user } });
};
