import { game } from ".prisma/client";
import { Category } from "../../interfaces";
import { prisma, Service } from "./index";
import { Game } from "../../interfaces";

/**
 * @returns Array of all existing Game instances
 * @async
 * @todo check "is_suggestion" while fetching games
 */
export const getAllGames: Service = async (
  search?: string,
  allGames?: boolean
): Promise<any> => {
  try {
    const games = await prisma.game.findMany({
      where: {
        ...(allGames ? {} : { is_suggestion: false }),
        ...(search && { name: { mode: "insensitive", contains: search } }),
      },
      include: {
        category: true,
        plays: {
          where: { offer_to_rent: true },
          include: { users: { include: { company: true } } },
        },
      },
      orderBy: { id: "desc" },
    });
    return games;
  } catch (error) {
    console.log("Failed to fetch games!");
    console.log(error);
    throw new Error("Failed to fetch games");
  }
};

/**
 * @returns Game instance according to passed ID
 * @async
 * @todo check "is_suggestion" while fetching games
 */
export const getGame: Service = async (id: number): Promise<any> => {
  try {
    return await prisma.game.findUnique({
      where: {
        id,
      },
      include: {
        category: true,
        plays: { include: { users: { include: { company: true } } } },
      },
    });
  } catch (error) {
    console.log("Failed to fetch game!");
    console.log(error);
    throw new Error("Failed to fetch game");
  }
};

export const createGame: Service = async (game: Game): Promise<game> => {
  return await prisma.game
    .create({
      data: {
        ...game,
        category: {
          connect: {
            id: game.category.id,
          },
        },
      },
    })
    .catch((error) => {
      console.log("Failed to create game!");
      console.log(error);
      throw new Error("Failed to create game!");
    })
    .catch((error) => {
      console.log("Failed to create game!");
      console.log(error);
      throw new Error("Failed to create game!");
    });
};

export const updateGame = (
  game: Omit<Game, "category"> & { category_id: number },
  id: number
) => {
  const { category_id, ...rest } = game;

  console.log(rest.is_suggestion);

  return prisma.game.update({
    where: { id },
    data: {
      ...rest,
      ...(category_id && {
        category: { connect: { id: category_id } },
      }),
    },
  });
};

export const approveGame: Service = async (id: number) => {
  return await prisma.game
    .update({
      where: {
        id: id,
      },
      data: {
        is_suggestion: false,
      },
    })
    .catch((error) => {
      console.log("Failed to approve game suggestion " + id + "!");
      console.log(error);
      throw new Error("Failed to approve game suggestion " + id + "!");
    });
};

export const deleteGame: Service = async (id: number) => {
  return await prisma.game
    .delete({
      where: {
        id: id,
      },
    })
    .catch((error) => {
      console.log("Failed to delete game " + id + "!");
      console.log(error);
      throw new Error("Failed to delete game " + id + "!");
    });
};

export const getCategories: Service = async (): Promise<Category[]> => {
  try {
    return await prisma.category.findMany({
      orderBy: {
        id: "asc",
      },
    });
  } catch (error) {
    console.log("Failed to fetch categories!");
    console.log(error);
    throw error;
  }
};

export const createCategory: Service = async (
  category: Category
): Promise<Category> => {
  try {
    return await prisma.category.create({
      data: {
        ...category,
      },
    });
  } catch (error) {
    console.log("Failed to add category " + category.name + "!");
    console.log(error);
    throw console.error;
  }
};

export const getCategory: Service = async (id: number): Promise<Category> => {
  try {
    return await prisma.category.findOne({
      where: {
        id: id,
      },
    });
  } catch (error) {
    console.log("Failed to find category!");
    console.log(error);
    throw error;
  }
};

export const deleteCategory: Service = async (
  id: number
): Promise<Category> => {
  try {
    return await prisma.category.delete({
      where: {
        id: id,
      },
    });
  } catch (error) {
    console.log("Failed to delete category " + id + "!");
    console.log(error);
    throw console.log(error);
  }
};

export const updateCategory: Service = async (
  id: number,
  newName: string
): Promise<Category> => {
  try {
    return await prisma.category.update({
      where: {
        id: id,
      },
      data: {
        name: newName,
      },
    });
  } catch (error) {
    console.log("Failed to update category " + id + "!");
    console.log(error);
    throw console.log(error);
  }
};
