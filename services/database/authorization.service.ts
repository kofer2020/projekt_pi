import { roles, users } from ".prisma/client";
import { prisma, Service } from ".";
import { LoginType, RegistrationType } from "../../interfaces";
import { v4 as uuid } from "uuid";
import * as bcrypt from "bcrypt";
import { company, usersUpdateInput } from "@prisma/client";

export const companyExists = async (name: string) => {
    return await prisma.company.findUnique({ where: { name } });
};

export const createUser: Service = async (
    registrationData: RegistrationType
  ): Promise<users> => {
      const {password, username, email, isCompany} = registrationData;
  
    const hashed_password = await bcrypt.hash(password, 10);
    const userData: any = {
      username: username,
      email: email,
      hashed_password,
      is_active: false,
      user_role: roles.User,
      token: uuid(),
      
    };
  
    let company = {}
    if (isCompany) {
      const { companyName, companyId } = registrationData;
      userData.user_role = roles.Renter;
      userData.is_accepted = false;
  
      if (companyId) {
        company = { connect: { id: companyId } };
      } else {
        const alreadyExists = await companyExists(companyName);
  
        if(alreadyExists) {
  
          company = { connect: { id: companyId } };
        } else {
          company = {
            create: {
              name: registrationData.companyName,
              address: registrationData.companyAddress,
              telephone: registrationData.companyTelephone,
            },
          };
        }
      }
    }
  
    return prisma.users.create({data: {...userData, company }})
  };
  
  

export const userExists: Service = async (email: string): Promise<users> => {
        const result = await prisma.users.findOne({
            where: {
                email: email
            }
        })
            .catch(e => {
                console.log("User fetching error")
                console.error(e)
                throw e
            })
            .finally(async () => {
                await prisma.$disconnect()
            })

        if (result == null) {
            throw new Error("Failed to fetch user!");
        }

        return result;
    };

    /**
     * @todo Can't remove unnecesery data because dono how tho check pw and active later?
     * @param userData 
     */
    export const login: Service = async (userData: LoginType): Promise<usersUpdateInput | users> => {
        const result = await prisma.users
            .findUnique({ where: { email: userData.email } })
            .catch((e) => {
                console.log("User login error!");
                console.error(e);
                throw e;
            })
            .finally(async () => {
                await prisma.$disconnect();
            });

        if (result == null) {
            throw new Error("Login failed. Please try again!");
        }

        console.log("Login requested for user: ");
        console.log(result);

        const passwordCorrect = await bcrypt.compare(
            userData.password,
            result.hashed_password
        );
        if (!passwordCorrect) {
            throw new Error("Incorrect password!");
        }

        if (result.is_active == false) {
            throw new Error("User is not activated, please check your email!");
        }

        return {
            id: result.id,
            email: result.email,
            username: result.username,
            company_id: result.company_id,
            photourl: result.photourl,
        };
    };

    export const activateUser: Service = async (userToken: string): Promise<users> => {
        const result = prisma.users
            .update({
                where: {
                    token: userToken,
                },
                data: {
                    is_active: true,
                },
            })
            .catch((e) => {
                console.log("User activation error!");
                console.error(e);
                throw e;
            })
            .finally(async () => {
                await prisma.$disconnect();
            });
        if (result == null) {
            throw new Error("User activation error!");
        }
        return result;
    };

    /**
     * Deletes user by its ID (cant delete admin)
     * @param id ID of user we want to delete
     * @returns deleted user
     * @async
     */
    export const deleteUser: Service = async (id: number): Promise<boolean> => {
        try {
            return await prisma.users.deleteMany({
                where: {
                    id,
                    user_role: {
                        notIn: [roles.Administrator]
                    }
                },
            }).then((arr) => {
                return arr.count > 0
            })
        } catch (error) {
            console.log("Cannot delete user " + id + "!")
            console.log(error)
            throw error
        }
    }
