import { users, usersUpdateInput } from "@prisma/client";
import { prisma, Service } from ".";
import { UpdateProfile, UserProfile } from "../../interfaces";

export const getUserProfile: Service = async (id: number):Promise<usersUpdateInput | users> => {
    return await prisma.users.findOne({
        where: {
            id
        },
        select:{
            id: true,
        email: true,
        username: true,
        company_id: true,
        photourl: true,
        }
    }).catch(e => {
        console.log("Fatal error! Fetching non-existing user from 'myProfile'")
        console.log("This should not happen")
        throw e;
    })
}

/**
 * Updates User/Moderator/Admin profile
 * @param id ID of user we want to update
 * @param user UpdateProfile type with changed data
 * @returns updated user
 * @async
 */
export const updateUserProfile: Service = async (id: number, user: UpdateProfile): Promise<users> => {
    try {
        return await prisma.users.update({
            where:{
                id
            },
            data:{
                ...user
            }
        })
    } catch (error) {
        console.log("Fatal error! Fetching non-existing user from 'myProfile'")
        console.log("This should not happen")
        throw error;
    }
}
