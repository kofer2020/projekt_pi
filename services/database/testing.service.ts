import { prisma, Service } from ".";

export const getUsers: Service = async () => {
    return await prisma.users.findMany()
        .catch(e => {
            console.log("User fetching error")
            throw e
        })
        .finally(async () => {
            await prisma.$disconnect()
        });
}

export const getCompanies: Service = async () => {
    return await prisma.company.findMany().catch(e => {
        console.log("Companies fetching error")
        throw e
    })
        .finally(async () => {
            await prisma.$disconnect()
        });
}

//Deletes all users
export const deleteUsers: Service = async () => {
    return await prisma.users.deleteMany({
        where:{
            id:{
                gt:0
            }
        }
    }).catch(e => {
        console.log("Companies fetching error")
        throw e
    })
        .finally(async () => {
            await prisma.$disconnect()
        });
}
export const deleteCompanies: Service = async () => {
    return await prisma.company.deleteMany({
        where:{
            id:{
                gt:0
            }
        }
    }).catch(e => {
        console.log("Companies fetching error")
        throw e
    })
        .finally(async () => {
            await prisma.$disconnect()
        });
}

