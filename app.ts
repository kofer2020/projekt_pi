import { decode } from "jsonwebtoken";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import routes from "./routes";
import { Request } from "./types";

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use("/", (req: Request, res, next) => {
  if (req.headers.authorization) {
    const token = req.headers.authorization.slice(7);
    const data = decode(token);

    const userId = (data as any)?.userId;

    if (userId) {
      req.userId = userId;
    }
  }

  next();
});

app.use('/api', routes);

export default app;
