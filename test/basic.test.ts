require('dotenv').config()
let request = require('supertest')

const url = "http://localhost:3001";
request = request(url)

describe('Auth', () => {
  it('POST /auth/signup - denies existing email', async(done)=>{
    const registrationBody = {
      email: process.env.ADMIN_EMAIL,
      password: "somePw",
      username: "someUser",
      isCompany: false,
    }
    const response = await request.post("/api/auth/signup").send(registrationBody)
    expect(response.status).toBe(400)
    expect(response.body).toBeDefined()
    expect(response.body.message).toBeDefined()
    done()
  })

  it('POST /auth/signup - denies existing username', async(done)=>{
    const loginBody = {
      email: "sureNotValidEmail",
      password: "sureNotHashOfSomePassword",
      username: process.env.ADMIN_USERNAME,
      isCompany: false
    }
    const response = await request.post("/api/auth/signup").send(loginBody)
    expect(response.status).toBe(400)
    expect(response.body).toBeDefined()
    expect(response.body.message).toBeDefined()
    done()
  })

  var newUserId = null;

  it('POST /auth/signup - register a new user', async(done)=>{
    const registrationBody = {
      email: "dummy4@mail",
      password: "dummyPassword",
      username: "dummy4",
      isCompany: false
    }

    const response = await request.post("/api/auth/signup").send(registrationBody)
    expect(response.status).toBe(201)
    newUserId = response.body.user.id
    done()
  })

  it('DELETE /user/:id - delete new user', async (done) => {
    const delResponse = await request.delete("/api/user/"+newUserId)
    expect(delResponse.status).toBe(200)
    done()
  })

  it('POST /auth/login - denies invalid login', async(done)=>{
    const loginBody = {
      email: "sureNotLegitEmail",
      password: "sureNotHashOfSomePassword"
    }
    const response = await request.post("/api/auth/login").send(loginBody)
    expect(response.status).toBe(400)
    expect(response.body).toBeDefined()
    expect(response.body.message).toBeDefined()
    done()
  })

  it('POST /auth/login - denies invalid password', async(done)=>{
    const loginBody = {
      email: process.env.ADMIN_EMAIL,
      password: "sureNotHashOfSomePassword"
    }
    const response = await request.post("/api/auth/login").send(loginBody)
    expect(response.status).toBe(400)
    expect(response.body).toBeDefined()
    expect(response.body.message).toBeDefined()
    done()
  })

  it('POST /auth/login - approves admin login', async(done)=>{
    const loginBody = {
      email: process.env.ADMIN_EMAIL,
      password: process.env.ADMIN_PW,
    }
    const response = await request.post("/api/auth/login").send(loginBody)
    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    done()
  })
})


describe('Category', () => {
  it('GET /category - list all categories', async (done) => {
    const getResponse = await request.get("/api/category")
    expect(getResponse.status).toBe(200)
    expect(getResponse.body).toBeDefined()
    done()
  })

  var id = null;

  it('POST /category - create new category', async (done) => {
    const categoryBody = {
      name: 'DUMMY CATEGORY 7'
    }
    const postResponse = await request.post("/api/category").send(categoryBody)
    expect(postResponse.status).toBe(201)
    id = postResponse.body.id
    done()
  })

  it('GET /category/:id - get category by id', async (done) => {
    const getResponse = await request.get("/api/category/"+id)
    expect(getResponse.status).toBe(200)
    done()
  })

  it('PUT /category/:id - update category', async (done) => {
    const updatedCategoryBody = {
      name: 'DUMMY CATEGORY 7U'
    }
    const putResponse = await request.put("/api/category/"+id).send(updatedCategoryBody)
    expect(putResponse.status).toBe(200)
    done()
  })

  it('DELETE /category/:id - delete category', async (done) => {
    const delResponse = await request.delete("/api/category/"+id)
    expect(delResponse.status).toBe(200)
    done()
  })
})

describe('Game', () => {
  it('GET /game - list all games', async (done) => {
    const getResponse = await request.get("/api/game")
    expect(getResponse.status).toBe(200)
    expect(getResponse.body).toBeDefined()
    done()
  })

  var id = null;

  it('POST /game - create new game', async (done) => {
    const gameBody = {
      name: 'Dummy Game 7',
      description: 'Dummy Description',
      max_players: 6,
      duration: 60, 
      link: 'Dummy Link', 
      no_of_copies: 1,
      category: {id: 4, name: 'Strategy'},
      is_suggestion: true,
    }
    const postResponse = await request.post("/api/game").send(gameBody)
    expect(postResponse.status).toBe(201)
    id = postResponse.body.id
    done()
  })

  it('GET /game/:id - get game by id', async (done) => {
    const getResponse = await request.get("/api/game/"+id)
    expect(getResponse.status).toBe(200)
    expect(getResponse.body).toBeDefined()
    done()
  })

  it('GET /game/approve/:id - approve game by id', async (done) => {
    const approveResponse = await request.get("/api/game/approve/"+id)
    expect(approveResponse.status).toBe(200)
    done()
  })

  it('DELETE /game/:id - delete game', async (done) => {
    const delResponse = await request.delete("/api/game/"+id)
    expect(delResponse.status).toBe(200)
    done()
  })
})

describe('Group', () => {
  it('GET /group - list all groups', async (done) => {
    const response = await request.get('/api/group')
    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    done()
  })

  var id = null;

  it('POST /group - create new group', async (done) => {
    const groupBody = {
      capacity: 6,
      name: "Test Group 2",
      owner_id: 1,
      game: {id: 1},
    }
    const response = await request.post('/api/group').send(groupBody)
    expect(response.status).toBe(201)
    expect(response.body).toBeDefined()
    id = response.body.id
    done()
  })

  it('GET /group/:id - get group by id', async (done) => {
    const response = await request.get('/api/group/'+id)
    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()
    done()
  })

  it('PUT /group/:id - update group', async (done) => {
    const groupBody = {
      capacity: 6,
      name: "Test Group 2U",
      owner_id: 1,
      game: {id: 1},
    }
    const response = await request.put('/api/group/'+id).send(groupBody)
    expect(response.status).toBe(200)
    done()
  })

  it('DELETE /group/:id - delete group', async (done) => {
    const response = await request.delete('/api/group/'+id)
    expect(response.status).toBe(200)
    done()
  })
})

