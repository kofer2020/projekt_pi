import { Request as BaseReq } from 'express';

export interface Request extends BaseReq {
  userId?: number
}