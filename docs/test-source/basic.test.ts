require('dotenv').config()
let request = require('supertest')

const url = "http://localhost:3001";
request = request(url)

describe('Static',() => {

  /** First, check if server responds */
  it('GET / - responds with anything', async (done) => {
    const response = await request.get('/')

    expect(response).not.toBeUndefined()

    done()
  })

  it('GET / - responds with 200 OK and html' ,async(done)=>{
    const response = await request.get('/')

    expect(response.status).toBe(200)
    expect(response.headers['content-type']).toBe('text/html; charset=UTF-8')

    done()
  })

})

describe('Auth', () => {

  it('POST /auth/login - denies invalid login', async(done)=>{
    const loginBody = {
      email: "sureNotLegitEmail",
      password: "sureNotHashOfSomePassword"
    }

    const response = await request.post("/auth/login").send(JSON.stringify(loginBody))
    
    expect(response.status).toBe(400)
    expect(response.body).toBeDefined()
    expect(response.body.message).toBeDefined()

    done()
  })

  it('POST /auth/login - denies invalid password', async(done)=>{
    const loginBody = {
      email: process.env.ADMIN_EMAIL,
      password: "sureNotHashOfSomePassword"
    }

    const response = await request.post("/auth/login").send(JSON.stringify(loginBody))
    
    expect(response.status).toBe(400)
    expect(response.body).toBeDefined()
    expect(response.body.message).toBeDefined()

    done()
  })

  it('POST /auth/login - approves admin login', async(done)=>{
    const loginBody = {
      email: process.env.ADMIN_EMAIL,
      password: process.env.ADMIN_PW,
    }

    const response = await request.post("/auth/login").send(loginBody)
    
    expect(response.status).toBe(200)
    expect(response.body).toBeDefined()

    done()
  })

  it('POST /auth/login - sets session cookie', async(done)=>{
    const loginBody = {
      email: process.env.ADMIN_EMAIL,
      password: process.env.ADMIN_PW,
    }

    const response = await request.post("/auth/login").send(loginBody)
    
    expect(response.status).toBe(200)
    expect(response.headers['set-cookie']).toBeDefined()

    done()
  })
})

describe('Signup', () => {

  it('POST /auth/signup - denies existing email', async(done)=>{
    const registrationBody = {
      email: process.env.ADMIN_EMAIL,
      password: "somePw",
      username: "someUser",
      isCompany: false
    }

    const response = await request.post("/auth/signup").send(JSON.stringify(registrationBody))
    
    expect(response.status).toBe(400)
    expect(response.body).toBeDefined()
    expect(response.body.message).toBeDefined()

    done()
  })

  it('POST /auth/signup - denies existing username', async(done)=>{
    const loginBody = {
      email: "sureNotValidEmail",
      password: "sureNotHashOfSomePassword",
      username: process.env.ADMIN_USERNAME,
      isCompany: false
    }

    const response = await request.post("/auth/signup").send(JSON.stringify(loginBody))
    
    expect(response.status).toBe(400)
    expect(response.body).toBeDefined()
    expect(response.body.message).toBeDefined()

    done()
  })

  it('POST /auth/signup - register and delete dummy user', async(done)=>{
    const registrationBody = {
      email: "dummy1@mail",
      password: "dummyPassword",
      username: "dummy1",
      isCompany: false
    }

    const response = await request.post("/auth/signup").send(registrationBody)
    
    expect(response.status).toBe(201)

    const delResponse = await request.delete("/user/"+response.body.id)
    expect(delResponse.status).toBe(200)
    done()
  })
})


