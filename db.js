const Pool = require("pg").Pool;
// Allows us to conifgure db connection while hiding our data!
require("dotenv").config();

const devConfig = {
   user: process.env.PG_USER,
   password: process.env.PG_PASSWORD,
   host: process.env.PG_HOST,
   database: process.env.PG_DATABASE,
   port: process.env.PG_PORT,
};

const productionConfig = process.env.DATABASE_URL;

const pool = new Pool({
    connectionString: process.env.NODE_ENV === "production" ? productionConfig : devConfig
});

module.exports = pool;