import next from "next";
import { join } from "path";
import app from "./app";

const PORT = parseInt(process.env.PORT || "", 10) || 3001;

if (process.env.NODE_ENV === "production") {
  const nextApp = next({ dev: false, dir: join(__dirname, "./client") });
  const handle = nextApp.getRequestHandler();

  // We choose environments port if it's specified, or 3000 as local port

  nextApp.prepare().then(() => {
    app.all("*", (req, res) => handle(req, res));
    app.listen(PORT, () => console.log(`> Ready on http://localhost:${PORT}`));
  });
} else {
  app.listen(PORT, () => console.log(`> Ready on http://localhost:${PORT}`));
}

declare namespace Express {
  export interface Request {
    userId?: number;
  }
}
