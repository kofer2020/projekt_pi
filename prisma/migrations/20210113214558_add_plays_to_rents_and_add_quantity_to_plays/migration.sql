/*
  Warnings:

  - The migration will change the primary key for the `rents` table. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `game_id` on the `rents` table. All the data in the column will be lost.
  - Added the required column `count` to the `plays` table without a default value. This is not possible if the table is not empty.
  - Added the required column `playsGame_id` to the `rents` table without a default value. This is not possible if the table is not empty.
  - Added the required column `playsPlayer_id` to the `rents` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "rents" DROP CONSTRAINT "rents_game_id_fkey";

-- AlterTable
ALTER TABLE "plays" ADD COLUMN     "count" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "rents" DROP CONSTRAINT "rents_pkey",
DROP COLUMN "game_id",
ADD COLUMN     "playsGame_id" INTEGER NOT NULL,
ADD COLUMN     "playsPlayer_id" INTEGER NOT NULL,
ADD PRIMARY KEY ("renter_id", "player_id", "game_instance");

-- AddForeignKey
ALTER TABLE "rents" ADD FOREIGN KEY("playsGame_id", "playsPlayer_id")REFERENCES "plays"("game_id","player_id") ON DELETE CASCADE ON UPDATE CASCADE;
