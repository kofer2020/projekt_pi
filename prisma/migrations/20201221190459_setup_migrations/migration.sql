-- CreateEnum
CREATE TYPE "roles" AS ENUM ('User', 'Renter', 'Moderator', 'Administrator');

-- CreateEnum
CREATE TYPE "invitation_states" AS ENUM ('Pending', 'Accepted', 'Rejected');

-- CreateEnum
CREATE TYPE "request_states" AS ENUM ('Pending', 'Accepted', 'Rejected');

-- CreateTable
CREATE TABLE "company" (
"id" SERIAL,
    "name" TEXT NOT NULL,
    "web_address" TEXT,
    "address" TEXT NOT NULL,
    "telephone" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "users" (
"id" SERIAL,
    "username" TEXT NOT NULL,
    "hashed_password" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "photourl" TEXT,
    "is_active" BOOLEAN NOT NULL,
    "token" TEXT NOT NULL,
    "token_created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "company_id" INTEGER,
    "user_role" "roles" NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ad" (
"id" SERIAL,
    "description" TEXT NOT NULL,
    "location" TEXT,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "deleted_at" TIMESTAMP(3),
    "group_id" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "category" (
"id" SERIAL,
    "name" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "game" (
"id" SERIAL,
    "name" TEXT NOT NULL,
    "photo_url" TEXT,
    "description" TEXT NOT NULL,
    "max_players" INTEGER NOT NULL,
    "duration" INTEGER NOT NULL,
    "link" TEXT NOT NULL,
    "category_id" INTEGER NOT NULL,
    "no_of_copies" INTEGER NOT NULL,
    "is_suggestion" BOOLEAN DEFAULT false,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "appointment" (
"id" SERIAL,
    "beginning" TIMESTAMP(3) NOT NULL,
    "duration" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "deleted_at" TIMESTAMP(3),
    "owner_id" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups" (
"id" SERIAL,
    "capacity" INTEGER NOT NULL,
    "name" TEXT,
    "owner_id" INTEGER NOT NULL,
    "description" TEXT,
    "game_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "deleted_at" TIMESTAMP(3),

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "invitation" (
    "group_id" INTEGER NOT NULL,
    "player_id" INTEGER NOT NULL,
    "description" TEXT,
    "sent_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "state" "invitation_states" DEFAULT E'Pending',
    "answered_at" TIMESTAMP(3),
    "expires_at" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("group_id","player_id")
);

-- CreateTable
CREATE TABLE "members" (
    "group_id" INTEGER NOT NULL,
    "player_id" INTEGER NOT NULL,
    "joined_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY ("group_id","player_id")
);

-- CreateTable
CREATE TABLE "messages" (
"id" SERIAL,
    "content" TEXT NOT NULL,
    "sent_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "deleted_at" TIMESTAMP(3),
    "player_id" INTEGER NOT NULL,
    "group_id" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "plays" (
    "game_id" INTEGER NOT NULL,
    "player_id" INTEGER NOT NULL,
    "offer_to_rent" BOOLEAN,

    PRIMARY KEY ("game_id","player_id")
);

-- CreateTable
CREATE TABLE "rents" (
    "game_id" INTEGER NOT NULL,
    "renter_id" INTEGER NOT NULL,
    "player_id" INTEGER NOT NULL,
    "game_instance" INTEGER NOT NULL,
    "when_ts" TIMESTAMP(3) NOT NULL,
    "price" DECIMAL(65,30) NOT NULL,
    "transaction_id" INTEGER NOT NULL,
    "processed_at" TIMESTAMP(3) NOT NULL,
    "expires_at" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("game_id","renter_id","player_id","game_instance")
);

-- CreateTable
CREATE TABLE "requests" (
    "player_id" INTEGER NOT NULL,
    "ad_id" INTEGER NOT NULL,
    "state" "request_states" DEFAULT E'Pending',

    PRIMARY KEY ("player_id","ad_id")
);

-- CreateTable
CREATE TABLE "review" (
    "author_id" INTEGER NOT NULL,
    "recipient_id" INTEGER NOT NULL,
    "opinion" TEXT NOT NULL,
    "rating" INTEGER NOT NULL,
    "written_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),

    PRIMARY KEY ("author_id","recipient_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "company.name_unique" ON "company"("name");

-- CreateIndex
CREATE UNIQUE INDEX "users.username_unique" ON "users"("username");

-- CreateIndex
CREATE UNIQUE INDEX "users.email_unique" ON "users"("email");

-- CreateIndex
CREATE UNIQUE INDEX "users.token_unique" ON "users"("token");

-- CreateIndex
CREATE UNIQUE INDEX "category.name_unique" ON "category"("name");

-- CreateIndex
CREATE UNIQUE INDEX "game.name_unique" ON "game"("name");

-- CreateIndex
CREATE UNIQUE INDEX "groups.name_unique" ON "groups"("name");

-- AddForeignKey
ALTER TABLE "users" ADD FOREIGN KEY("company_id")REFERENCES "company"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ad" ADD FOREIGN KEY("group_id")REFERENCES "groups"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "game" ADD FOREIGN KEY("category_id")REFERENCES "category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "appointment" ADD FOREIGN KEY("owner_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups" ADD FOREIGN KEY("game_id")REFERENCES "game"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups" ADD FOREIGN KEY("owner_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "invitation" ADD FOREIGN KEY("group_id")REFERENCES "groups"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "invitation" ADD FOREIGN KEY("player_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "members" ADD FOREIGN KEY("group_id")REFERENCES "groups"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "members" ADD FOREIGN KEY("player_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "messages" ADD FOREIGN KEY("group_id")REFERENCES "groups"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "messages" ADD FOREIGN KEY("player_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "plays" ADD FOREIGN KEY("game_id")REFERENCES "game"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "plays" ADD FOREIGN KEY("player_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "rents" ADD FOREIGN KEY("game_id")REFERENCES "game"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "rents" ADD FOREIGN KEY("player_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "rents" ADD FOREIGN KEY("renter_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "requests" ADD FOREIGN KEY("ad_id")REFERENCES "ad"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "requests" ADD FOREIGN KEY("player_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "review" ADD FOREIGN KEY("author_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "review" ADD FOREIGN KEY("recipient_id")REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
