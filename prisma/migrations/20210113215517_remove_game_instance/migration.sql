/*
  Warnings:

  - The migration will change the primary key for the `rents` table. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `game_instance` on the `rents` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "rents" DROP CONSTRAINT "rents_pkey",
DROP COLUMN "game_instance",
ADD PRIMARY KEY ("renter_id", "player_id");
