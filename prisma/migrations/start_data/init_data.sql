INSERT INTO users(username, hashed_password,email,is_active,is_accepted,token,user_role)
  VALUES (
    'madoxx',
    '$2b$10$vYwTTB3tRMFF0fotVqYejutLgDfp9QFNKfH4UEUcas/cojDfa3n3e',
    'ma@dox.hr',
    true,
    true,
    'xyz-token',
    'Administrator'
  );

INSERT INTO category(name) VALUES ('Strategy');
INSERT INTO category(name) VALUES ('Educational');
INSERT INTO category(name) VALUES ('Drinking');