CREATE TABLE locations(
    group_id INT NOT NULL,
    longitude DECIMAL(30,20) NOT NULL,
    latitude DECIMAL(30,20) NOT NULL,
    PRIMARY KEY(group_id,longitude,latitude),
    FOREIGN KEY(group_id) REFERENCES groups(id)
);