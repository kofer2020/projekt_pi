DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS company CASCADE;
DROP TYPE IF EXISTS roles CASCADE;
DROP TYPE IF EXISTS invitation_states CASCADE;
DROP TYPE IF EXISTS request_states CASCADE;
DROP TABLE IF EXISTS category CASCADE;
DROP TABLE IF EXISTS game CASCADE;
DROP TABLE IF EXISTS ad CASCADE;
DROP TABLE IF EXISTS groups CASCADE;
DROP TABLE IF EXISTS appointment CASCADE;
DROP TABLE IF EXISTS invitation CASCADE;
DROP TABLE IF EXISTS members CASCADE;
DROP TABLE IF EXISTS review CASCADE;
DROP TABLE IF EXISTS messages CASCADE;
--DROP TABLE IF EXISTS connections CASCADE;
DROP TABLE IF EXISTS plays CASCADE;
DROP TABLE IF EXISTS requests CASCADE;
DROP TABLE IF EXISTS rents CASCADE;

--Cannot be named 'role' because it is PGSQL keyword!
-- !IMPORTANT -> Defined roles are enumerated in that way so we can use operators < and > on them
CREATE TYPE roles AS ENUM ('User','Renter','Moderator','Administrator');
CREATE TYPE invitation_states AS ENUM('Pending','Accepted','Rejected');
CREATE TYPE request_states AS ENUM('Pending','Accepted','Rejected');

/*
web adresa moze biti null
*/
CREATE TABLE company (
  id SERIAL,
  name VARCHAR(50) NOT NULL,
  web_address VARCHAR(100),
  address VARCHAR(100) NOT NULL,
  telephone VARCHAR(15) NOT NULL,
  PRIMARY KEY(id),
  UNIQUE (name)
);
/*
preimenuj "mail" u "email"
*/
--Cannot be named 'user' because it is PGSQL keyword!
CREATE TABLE users (
  id SERIAL, 
  username VARCHAR(20) NOT NULL,
  hashed_password VARCHAR(100) NOT NULL,
  email VARCHAR(50) NOT NULL,
  photoURL VARCHAR(256),
  is_active BOOLEAN NOT NULL,
  token VARCHAR(100) NOT NULL,
  token_created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  company_id INT,
  user_role roles NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (company_id) REFERENCES COMPANY(id),
  UNIQUE (username),
  UNIQUE (email),
  UNIQUE (token)
);

CREATE TABLE category (
  id SERIAL,
  name VARCHAR(20) NOT NULL,
  PRIMARY KEY(id),
  UNIQUE (name)
);

/*
Postaviti ID na SERIAL, "photo" preimenovati u "photo_url" i staviti Varchar(256), photo_url moze biti NULL, link staviti VARCHAR(256)
dodati is_suggestion
*/
CREATE TABLE game (
  id SERIAL,
  name VARCHAR(100) NOT NULL,
  photo_url VARCHAR(256),
  description TEXT NOT NULL,
  max_players INT NOT NULL,
  duration INT NOT NULL,
  link VARCHAR(256) NOT NULL,
  category_id INT NOT NULL,
  no_of_copies INT NOT NULL,
  is_suggestion BOOLEAN DEFAULT FALSE,
  PRIMARY KEY(id),
  FOREIGN KEY(category_id) REFERENCES category(id),
  UNIQUE(name)
);

/**
  Admini nece imati svoje tablice, nego cemo samo na pocetku upisati u tablicu USERS neke korisnike sa rolom 'Administrator'
  (samo obrisi tablicu admins)
*/

/*
staviti da je ID tipa SERIAL
stavi da je "name" velicine 50
owner id nije serial..
dodati deleted_at tipa dateTime, moze biti null
!!!!
!!!! ne mozemo koristiti naziv 'group' jer je keyword
*/
CREATE TABLE groups (
  id serial,
  capacity INT NOT NULL,
  name VARCHAR(50),
  owner_id INT NOT NULL,
  description TEXT,
  game_id INT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  CONSTRAINT owner FOREIGN KEY(owner_id) REFERENCES users(id),
  FOREIGN KEY(game_id) REFERENCES game(id),
  PRIMARY KEY(id),
  UNIQUE(name)
);

/* 
Treba jos vidjeti kako cemo spremati lokaciju, za sada ostaviti tako
DeletedAt može bit NULL
OwnerID nije serial..
*/
CREATE TABLE ad (
  id serial,
  description TEXT NOT NULL,
  location VARCHAR(100),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  group_id INT NOT NULL,
  FOREIGN KEY(group_id) REFERENCES groups(id),
  PRIMARY KEY(id)
);

/* 
ne mogu koristiti naziv tablice "message" jer je keyword...
id je serial 
opet ne mogu koristiti naziv "text", nazovi ga "content"
player_id nije serial
*/
CREATE TABLE messages (
  id serial,
  content TEXT NOT NULL,
  sent_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  player_id INT NOT NULL,
  group_id INT NOT NULL,
  FOREIGN KEY(player_id) REFERENCES users(id),
  FOREIGN KEY(group_id) REFERENCES groups(id),
  PRIMARY KEY (id)
);

/*
 id mora biti serial
 owner_id nije serial nego obicni int
 deleted_at moze bit NULL
*/
CREATE TABLE appointment (
  id serial,
  beginning TIMESTAMP NOT NULL,
  duration INT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  owner_id INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(owner_id) REFERENCES users(id)
);

/*
player_id nije serial
ne mogu koristiti naziv "text" jer je keyword.. preimenuj u "description", može biti NULL
preimenuj end_at u expires_at, intuitivnije je
*/
CREATE TABLE invitation (
  group_id INT,
  player_id INT,
  description TEXT,
  sent_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  state invitation_states DEFAULT 'Pending',
  answered_at TIMESTAMP,
  expires_at TIMESTAMP NOT NULL,

  FOREIGN KEY(group_id) REFERENCES groups(id),
  FOREIGN KEY(player_id) REFERENCES users(id),
  PRIMARY KEY(group_id,player_id)
);

/*
player_id nije serial
*/
CREATE TABLE members (
  group_id INT,
  player_id INT,
  joined_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY(group_id) REFERENCES groups(id),
  FOREIGN KEY(player_id) REFERENCES users(id),
  PRIMARY KEY(group_id,player_id)
);
/*
author_id i recipient_id nisu serial
rejting je INT
*/
CREATE TABLE review (
  author_id INT,
  recipient_id INT,
  opinion TEXT NOT NULL,
  rating INT NOT NULL,
  written_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP,
  PRIMARY KEY(author_id,recipient_id),
  FOREIGN KEY(author_id) REFERENCES users(id),
  FOREIGN KEY(recipient_id) REFERENCES users(id)
);

/*
player_id nije serial
*/
CREATE TABLE plays (
  game_id INT,
  player_id INT,
  offer_to_rent BOOLEAN,
  PRIMARY KEY(game_id,player_id),
  FOREIGN KEY(game_id) REFERENCES game(id),
  FOREIGN KEY(player_id) REFERENCES users(id)
);

/*
player_id nije serial
!!!!
opis fali u dokumentaciji, definirati koja stanja mogu bit kod requesta
*/
CREATE TABLE requests (
  player_id INT,
  ad_id INT,
  state request_states DEFAULT 'Pending',
  PRIMARY KEY(player_id,ad_id),
  FOREIGN KEY(player_id) REFERENCES users(id),
  FOREIGN KEY(ad_id) REFERENCES ad(id)
);

/*
renter_id i player_id nisu serial
!!!!!
opis fali u dokumentaciji, 
sto je tocno game_instance?
ne mogu koristiti 'when' jer je keyword.. dodato sam 'ts' kao 'timestamp'
payPal ?? mislim da ovo ne trebamo jer smo izbacili druge nacine placanja
*/
CREATE TABLE rents (
  game_id INT,
  renter_id INT,
  player_id INT,
  game_instance INT,
  when_ts TIMESTAMP NOT NULL,
  price DECIMAL(8,2) NOT NULL,
  transaction_id INT NOT NULL,
  processed_at TIMESTAMP NOT NULL,
  expires_at TIMESTAMP NOT NULL,
  PRIMARY KEY (game_id,renter_id,player_id,game_instance),
  FOREIGN KEY (game_id) REFERENCES game(id),
  FOREIGN KEY (renter_id) REFERENCES users(id),
  FOREIGN KEY (player_id) REFERENCES users(id)
);

