import {Router} from "express";
import { prisma } from "../services/database";
import {
  createGroup,
  deleteGroup,
  getAllGroups,
  getGroup,
  getInvitationsForGroup,
  resolveInvitation,
  updateGroup,
} from "../services/database/groups.service";
import { Request } from "../types";

const groupRouter = Router();

groupRouter.get('/', async (req, res) => {
    getAllGroups()
        .then(groups => res.status(200).json(groups))
        .catch(() => res.status(400).json({message:"Failed to load groups!"}))
})

groupRouter.post('/', async (req, res) => {
    createGroup(req.body)
        .then(group => res.status(201).json(group))
        .catch(() => res.status(400).json({message:"Failed to create group!"}))
})

groupRouter.get('/:id', async (req, res) => {
    const id: number = Number(req.params.id)
    getGroup(id)
        .then(group => res.status(200).json(group))
        .catch(() => res.status(400).json({message:"Failed to load group " + req.params.id + "!"}))
})

groupRouter.delete('/:id', async (req, res) => {
    const id: number = Number(req.params.id)
    deleteGroup(id)
        .then(() => res.status(200).json({message:"Successfully deleted group!"}))
        .catch(() => res.status(400).json({message:"Failed to delete group " + id + "!"}))
})

groupRouter.put('/:id', async (req, res) => {
    const id: number = Number(req.params.id)
    req.body.id = id;
    updateGroup(req.body)
        .then(group => res.status(200).json(group))
        .catch(() => res.status(400).json({message:"Failed to update group!"}))
})

groupRouter.get('/:id/invitations', async (req, res) => {
    const id: number = Number(req.params.id)
    getInvitationsForGroup(id)
        .then(invitations => res.status(200).json(invitations))
        .catch(() => res.status(400).json({message:"Failed to get group invitations!"}))
})

groupRouter.post('/resolve_invite', async (req, res) => {
    resolveInvitation(req.body)
        .then(invitation => res.status(200).json(invitation))
        .catch(() => res.status(400).json({message:"Failed to resolve invitation!"}))
})

groupRouter.post("/marker/:id", async (req, res) => {
  const { longitude, latitude } = req.body;
  const group_id: number = parseInt(req.params.id);

  console.log('here');

  try {
    let marker = await prisma.locations.findFirst({ where: { group_id } });
    if (!marker) {
      marker = await prisma.locations.create({
        data: {
          group: {
            connect: {
              id: group_id,
            },
          },
          latitude,
          longitude,
        },
      });
    } else {
      marker = await prisma.locations.update({
        where: {
          group_id_longitude_latitude: {
            group_id,
            latitude: marker.latitude,
            longitude: marker.longitude,
          },
        },
        data: {
          latitude,
          longitude,
        },
      });
    }

    res.json(marker);
  } catch (err) {
    console.log(err)
    res.status(500).json(err);
  }
});

groupRouter.delete('/marker/:id', async (req,res)=>{
  const {longitude, latitude} = req.body;
  const group_id: number = parseInt(req.params.id)
  await prisma.locations.delete({
    where:{
      group_id_longitude_latitude:{
        group_id,
        longitude,
        latitude,
      }
    }
  })
  .then((marker)=> res.json(marker))
  .catch((err)=>res.status(500).json(err))
})

groupRouter.get('/marker/:id', async (req,res)=>{
  const group_id: number = parseInt(req.params.id)
  await prisma.locations.findMany({
    where:{
      group_id
    },
    select:{
      longitude:true,
      latitude:true,
    }
  })
  .then((marker)=> res.json(marker.length === 1 ? marker[0] : null))
  .catch((err)=>res.status(500).json(err))
})

groupRouter.post('/:groupId/:userId', async (req: Request, res) => {
    try {
      const group = await prisma.groups.findUnique({
        where: { id: parseInt(req.params.groupId) },
        include: {members: true}
      });

      if (!group) {
        return res.status(403).json({ error: "Group not found" });
      }

      await prisma.members.create({
        data: {
          users: { connect: { id: parseInt(req.params.userId) } },
          groups: { connect: { id: group.id } },
        },
      });

      res.status(200).json({ ok: true });
    } catch (err) {
      res.status(499).json(err);
    }
})

groupRouter.delete('/:id/:userId', async (req, res) => {
    try {
      await prisma.members.deleteMany({
        where: {
          player_id: parseInt(req.params.userId),
          group_id: parseInt(req.params.id),
        },
      });

      res.status(201).json({ ok: true });
    } catch (err) {
      res.status(500).json(err);
    }
})




export default groupRouter;
