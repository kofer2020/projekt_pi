import { Router } from "express";
import path = require("path");

const staticRouter = Router();

/**
 * Returns login page HTML
 */
staticRouter.get("/login", (req, res) => {
    console.log("Returning login.html!");
    res.sendFile(path.join(__dirname, "../client/out/login.html"));
});

/**
 * Returns signup page HTML
 */
staticRouter.get("/signup", (req, res) => {
    console.log("Returning signup.html!");
    res.sendFile(path.join(__dirname, "../client/out/signup.html"));
});

/**
 * Returns home page HTML
 */
staticRouter.get("/home", (req, res) => {
    console.log("Returning home.html!");
    res.sendFile(path.join(__dirname, "../client/out/home.html"));
});

/**
 * Returns landing page HTML
 */
staticRouter.get("/", (req, res) => {
    console.log("Returning index.html!");
    res.sendFile(path.join(__dirname, "../client/out/index.html"));
});

export default staticRouter;