import { Prisma, review } from "@prisma/client"
import {Router} from "express"
import { prisma } from "../services/database"
import { Request } from "../types"

export const reviewRouter = Router()

/** Return all reviews where user under passed id is RECEIPIENT */
reviewRouter.get(`/:id`,(req: Request, res) => {
    const recipient_id = parseInt(req.params.id)
    prisma.review.findMany({
        where:{
            recipient_id
        }
    })
    .then((reviews) => res.json(reviews))
    .catch((err)=> res.status(500).json(err))
})

/** creates new review */
reviewRouter.post(`/`,(req: Request, res)=>{
    const review: review = req.body
    prisma.review.create({
        data:{
            ...review,
            users_review_author_idTousers:{
                connect:{
                    id: review.author_id
                }
            },
            users_review_recipient_idTousers:{
                connect:{
                    id: review.recipient_id
                }
            }
        }
    })
    .then((review)=> res.json(review))
    .catch((err)=> res.status(500).json(err))
})

/** review update */
reviewRouter.put(`/`,(req:Request, res)=>{
    const {rating, opinion, author_id
    , recipient_id} = req.body
    prisma.review.update({
        where:{
            author_id_recipient_id:{
                author_id,
                recipient_id
            }
        }, data:{
            ...(rating && {rating}),
            ...(opinion && {opinion}),
            updated_at: new Date()
        }
    })
    .then((review)=> res.json(review))
    .catch((err)=> res.status(500).json(err))
})

