import { Router } from "express";
import { prisma } from "../services/database/index";
import { Request } from "../types";

export const groupMessageRouter = Router();

groupMessageRouter.get("/:id", async (req, res) => {
  try {
    const groupMembers = await prisma.groups.findUnique({
      where: { id: parseInt(req.params.id) },
      include: {
        members: {
          include: { users: { select: { username: true, id: true } } },
        },
        owner: { select: { id: true, username: true } },
      },
    });

    if (!groupMembers) {
      return res.status(404).json({ error: "Can't find group" });
    }

    const messages = await prisma.messages.findMany({
      where: { group_id: parseInt(req.params.id) },
      include: { users: true },
      orderBy: { id: "asc" },
    });

    res.json({ groupMembers, messages });
  } catch (err) {
    res.status(500).json(err);
  }
});

groupMessageRouter.post("/:id", async (req: Request, res) => {
  const { content } = req.body;

  try {
    const group = await prisma.groups.findUnique({
      where: { id: parseInt(req.params.id) },
    });

    await prisma.messages.create({
      data: {
        content,
        users: { connect: { id: req.userId } },
        groups: { connect: { id: group.id } },
      },
    });

    res.json({ ok: true });
  } catch (err) {
    res.status(500).json(err);
  }
});
