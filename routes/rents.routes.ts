import { rents } from "@prisma/client";
import {Router} from "express"
import { prisma } from "../services/database"
import { Request } from "../types";

export const rentsRouter = Router()

/** Get all VALID i.e. not-expired rents for a specified player
 * @todo should we just return games, not rents?
 */
rentsRouter.get("/:id",async (req,res)=>{
    const player_id: number = parseInt(req.params.id);
    prisma.rents.findMany({
        where:{
            player_id,
            expires_at:{
                gt: new Date()
            }
        }
    })
    .then((rents)=>res.json(rents))
    .catch((err)=> res.status(500).json(err))
})

/** Get all rents from specified renter */
rentsRouter.get("/renter/:id",async (req,res)=>{
    const renter_id: number = parseInt(req.params.id);
    prisma.rents.findMany({
        where:{
            renter_id
        }
    })
    .then((rents)=>res.json(rents))
    .catch((err)=> res.status(500).json(err))
})

rentsRouter.post(`/`, async (req:Request,res) => {
    const rent: rents = req.body;
    prisma.rents.create({
        data:{
            ...rent,
            plays:{
                connect:{
                    game_id_player_id:{
                        game_id: rent.playsGame_id,
                        player_id: rent.playsPlayer_id,
                    }
                }
            },
            users_rents_player_idTousers:{
                connect:{
                    id: rent.player_id
                }
            },
            users_rents_renter_idTousers:{
                connect:{
                    id: rent.renter_id
                }
            }
        },
    })
    .then((rent)=>res.json(rent))
    .catch((err)=>res.status(500).json(err))
})