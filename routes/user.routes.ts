import {Router} from "express";
import { deleteUser } from "../services/database/authorization.service";
import {getUserProfile} from "../services/database/profile.service";

const userRouter = Router();

/**
 * Returns user associated with current session
 */
userRouter.get('/current', async (req, res) => {
    // @ts-expect-error
    if (req.session.loggedIn) {
        // @ts-expect-error
        getUserProfile(req.session.user.id)
            .then(user => res.status(200).json(user))
            .catch(() => res.status(500).json({message: "Error when fetching user!"}))
    } else {
        res.status(400).json({message: "User not logged in!"})
    }
});

userRouter.delete('/:id', async (req,res) => {
    const userID:number = Number(req.params.id)
    try {
        deleteUser(userID).then((deleted)=>{
            deleted ? res.sendStatus(200): res.sendStatus(404)
        }).catch(()=> res.status(400).json({message: "User cannot be deleted!"}))
    } catch (error) {
        console.log("Cannot delete user")
    }
})

export default userRouter;
