import {sign} from "jsonwebtoken";
import { Router } from "express";
import { createTransport } from "nodemailer";

import { prisma } from "../services/database";
import { Request} from '../types'
import { RegistrationType } from "../interfaces";
import {
  activateUser,
  createUser,
  deleteUser,
  login,
} from "../services/database/authorization.service";

const transporter = createTransport({
  service: 'FastMail',
  auth: {
    user: "kofer_gameex@FastMail.com",
    pass: "55cgq47asf3jxepa",
  }
});

const authRouter = Router();

/**
 * Verifies user credentials and maps user to session (sets session cookie)
 */
authRouter.post(`/login`, async (req, res) => {
  try {
    const user = await login(req.body);
    const token = sign({ userId: user.id }, "secret");

    res.status(200).json({ user, token });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

authRouter.get("/companies", (req, res) => {
  prisma.company
    .findMany()
    .then((comapnies) => res.json(comapnies))
    .catch((err) => res.status(500).json(err));
});


authRouter.get('/me', async (req: Request, res) => {
    if(req.userId) {
        const user = await prisma.users.findUnique({
          where: { id: req.userId },
          include: {
            groups: true,
            plays: true,
            company: true,
            members: { include: { groups: true } },
          },
        });

        return res.json(user)
    }

    return res.status(401).json({error: 'not logged in'})
})

/**
 * Creates new inactive user using parameters from body
 */
authRouter.post(`/signup`, async (req, res) => {
  try {
    const userData: RegistrationType = req.body;
    const user = await createUser(userData);
    // Activation URL to send via email
    const fullUrl =
      req.protocol + "://" + req.headers.host + "/api/auth/verify/" + user.token;
    console.log(fullUrl);

    const mailOptions = {
      from: "kofer_gameex@fastmail.com",
      to: user.email,
      subject: "koFER Account Verification",
      text:
        "Hello " +
        user.username +
        ",\n\nPlease click on the following link to activate your account:\n" +
        fullUrl,
    };

    await transporter.sendMail(mailOptions);

    const token = sign({ userId: user.id }, "secret");

    res.status(201).json({ user, token });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

/**
 * Activates user with token specified in the URL
 */
authRouter.get(`/verify/:token`, async (req, res) => {
  try {
    await activateUser(req.params.token);
    res.status(200).json({ message: "Successfully activated user!" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

/**
 * Deletes user from DB
 * @todo admin shoul not be deleted
 */
authRouter.delete(`/:id`, async (req, res) => {
  const userID: number = Number(req.params.id);
  deleteUser(userID).then(
    (isDeleted) => {
      if (isDeleted)
        res
          .status(200)
          .json({ message: "User successfully deleted!" })
          .sendFile("client/out/index.html");
      else res.status(200).json({ message: "Cannot delete Admin!" });
    },
    (err) => {
      res.status(400).json({ message: err.message });
    }
  );
});

export default authRouter;
