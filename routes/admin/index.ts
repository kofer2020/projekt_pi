import { Router } from "express";
import { getAllGames } from "../../services/database/game.service";
import { adminUsersRouter } from "./users.routes";

export const adminRouter = Router();

adminRouter.use("/users", adminUsersRouter);
adminRouter.get("/games", (req, res) => {
  getAllGames(undefined, true)
    .then((games) => {
      res.status(200).json(games);
    })
    .catch((error) => res.status(400).json({ message: error.message }));
});
