import { roles } from "@prisma/client";
import { Router } from "express";
import {
    acceptUser,
    getAllUsers,
    getUser,
    updateUser,
} from "../../services/database/admin/user.service";
import { deleteUser } from "../../services/database/authorization.service";
import { Request } from "../../types";

export const adminUsersRouter = Router();

adminUsersRouter.get("/", (req, res) => {
    getAllUsers()
        .then((users) => res.json(users))
        .catch((err) => res.status(500).json(err));
});

adminUsersRouter.get("/me", (req: Request, res) => {
    getUser(req.userId)
        .then((user) => res.json(user))
        .catch((err) => res.status(500).json(err));
});


adminUsersRouter.get("/:id", (req, res) => {
    getUser(parseInt(req.params.id))
        .then((user) => res.json(user))
        .catch((err) => res.status(500).json(err));
});

adminUsersRouter.delete("/:id", (req,res)=>{
    deleteUser(parseInt(req.params.id))
        .then(()=> res.send(200))
        .catch((err)=> res.status(500).json(err))
})


adminUsersRouter.patch("/:id/accept", (req, res) => {
    acceptUser(parseInt(req.params.id))
        .then((user) => res.json(user))
        .catch((err) => res.status(500).json(err));
});

const rolesStrings = ["Moderator", "Renter", "Administrator", "User"] as roles[];
adminUsersRouter.patch("/:id", (req, res) => {
    if (req.body.user_role && !rolesStrings.includes(req.body.user_role)) {
        return res.status(400).json({ error: "Role not found" });
    }

    updateUser(parseInt(req.params.id), req.body)
        .then((user) => res.json(user))
        .catch((err) => res.status(500).json(err));
});

