import { Router } from "express";
import {
  getPlays,
  addPlay,
  updatePlay,
  deletePlay,
} from "../services/database/plays.service";
import { Request } from "../types";

export const playsRouter = Router();

playsRouter.get("/", (req: Request, res) => {
  // get own plays
  getPlays(req.userId)
    .then((plays) => res.json(plays))
    .catch((err) => res.status(500).json(err));
});

playsRouter.get("/:id", (req, res) => {
  getPlays(parseInt(req.params.id))
    .then((plays) => res.json(plays))
    .catch((err) => res.status(500).json(err));
});

playsRouter.post("/", (req: Request, res) => {
  const { gameId, offerToRent, price, count } = req.body;
  addPlay(req.userId, gameId, offerToRent, price, count || 1)
    .then((play) => res.json(play))
    .catch((err) => {
      console.log(err);
      res.status(500).json(err);
    });
});

playsRouter.patch("/", (req: Request, res) => {
  const { gameId, offerToRent, price, count } = req.body;
  updatePlay(req.body.player_id || req.userId, gameId, offerToRent, price, count)
    .then((plays) => res.json(plays))
    .catch((err) => {
      console.log(err);
      res.status(500).json(err);
    });
});

playsRouter.delete("/", (req: Request, res) => {
  const { gameId } = req.body;
  deletePlay(req.userId, gameId)
    .then((plays) => res.json(plays))
    .catch((err) => res.status(500).json(err));
});
