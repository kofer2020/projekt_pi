import {Router} from "express";
import {Game} from "../interfaces";
import {approveGame, createGame, deleteGame, getAllGames, getGame, updateGame} from "../services/database/game.service";


const gameRouter = Router();

/**
 * Returns array of all games
 */
gameRouter.get('/', (req, res) => {
    getAllGames()
    .then(games => {
        res.status(200).json(games)
    })
    .catch((error) => res.status(400).json({ message: error.message }));
});

/**
 * Returns Game instance (by name)
 */
gameRouter.get('/:id', (req, res) => {
    const gameId: number = Number(req.params.id)
    getGame(gameId)
    .then(game => res.status(200).json(game))
    .catch(error => {
        console.log(error)
        res.status(400).json({message: error.message})
    })
})

/**
 * Creates a new game from parameters passed in body
 */
gameRouter.post('/',(req, res) => {
    const gameData: Game = req.body
    console.log('Create game request: ')
    console.log(gameData)
    createGame(gameData)
        .then(game => res.status(201).json(game))
        .catch(error => {
            console.log(error)
            res.status(400).json({message: error.message})
        })
})

/**
 * Changes the is_suggestion value to false for game with 'id' specified in URL
 */
gameRouter.post('/approve/:id', (req, res) => {
    const id: number = Number(req.params.id)
    approveGame(id)
        .then(game => res.status(200).json(game))
        .catch(() => res.status(400).json({message: "Failed to approve game " + id + "!"}))
})

/**
 * Deletes the game with 'id' specified in URL
 */
gameRouter.delete('/:id', (req, res) => {
    const id: number = Number(req.params.id)
    deleteGame(id)
        .then(() => res.status(200).json({message: "Deleted successfully!"}))
        .catch(() => res.status(400).json({message: "Failed to delete game " + req.params.id + "!"}))
})

export default gameRouter;
