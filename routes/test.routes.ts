import { Router } from 'express';

import {
    deleteCompanies,
    deleteUsers,
    getCompanies,
    getUsers
} from "../services/database/testing.service";

const testRouter = Router();

testRouter.get("/users", async (req, res) => {
    const result = await getUsers();
    res.json(result);
});

testRouter.get("/companies", async (req, res) => {
    const result = await getCompanies();
    res.json(result);
});

testRouter.delete("/users", async (req, res) => {
    const result = await deleteUsers();
    res.json(result);
});

testRouter.delete("/companies", async (req, res) => {
    const result = await deleteCompanies();
    res.json(result);
});

testRouter.get("/session", async (req, res) => {
    res.json(req.session);
});

export default testRouter;