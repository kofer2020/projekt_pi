import { Router } from "express";
import { prisma } from "../services/database/index";
import { Request } from "../types";

export const chatRouter = Router();

chatRouter.get("/:id", (req, res) => {
  prisma.chat
    .findUnique({
      where: { id: parseInt(req.params.id) },
      include: {
        messages: { include: { users: true }, orderBy: { id: "asc" } },
        user1: true,
        user2: true,
      },
    })
    .then((chat) => {
      res.json(chat);
    })
    .catch((err) => res.status(500).json(err));
});

chatRouter.get("/", (req: Request, res) => {
  prisma.chat
    .findMany({
      where: { OR: [{ userId1: req.userId }, { userId2: req.userId }] },
    })
    .then((chat) => {
      res.json(chat);
    })
    .catch((err) => res.status(500).json(err));
});

chatRouter.post("/:id", async (req: Request, res) => {
  const { content } = req.body;

  try {
    console.log(req.params)

    const chat = await prisma.chat.findUnique({
      where: { id: parseInt(req.params.id) },
    });

    await prisma.messages.create({
      data: {
        content,
        users: { connect: { id: req.userId } },
        Chat: { connect: { id: chat.id } },
      },
    });

    res.json({ ok: true });
  } catch (err) {
    res.status(500).json(err);
  }
});

chatRouter.post("/", (req: Request, res) => {
  console.log(req.userId);
  console.log(req.body);

  const { otherId } = req.body;

  const data = {
    user1: { connect: { id: req.userId } },
    user2: { connect: { id: parseInt(otherId) } },
  };

  console.log(data);

  prisma.chat
    .create({ data })
    .then((chat) => {
      res.status(201).json(chat);
    })
    .catch((err) => res.status(500).json(err));
});
