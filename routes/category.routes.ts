import { Router } from "express";
import { Category } from "../interfaces";
import { prisma } from "../services/database";
import { createCategory, deleteCategory, getCategories, getCategory, updateCategory } from "../services/database/game.service";


const categoryRouter = Router();

/**
 * Returns list of all existing categories
 */
categoryRouter.get("/", (req, res) => {
  getCategories()
    .then((categories) => res.status(200).json(categories))
    .catch(() =>
      res.status(400).json({ message: "Failed to fetch categories" })
    );

})

categoryRouter.get("/:id", (req, res) => {
  prisma.category.findUnique({where: {id: parseInt(req.params.id)}})
    .then((category) => res.status(200).json(category))
    .catch(() =>
      res.status(400).json({ message: "Failed to fetch category" })
    );
});

/**
 * Creates new category from data passed in body
 */
categoryRouter.post('/', (req, res) => {
    const categoryData: Category = req.body
    createCategory(categoryData)
        .then(categories => res.status(201).json(categories))
        .catch(() => res.status(400).json({ message: "Failed to create category: "+categoryData.name+"!" }))
})

/**
 * Returns category by id
 */
categoryRouter.get('/:id', (req, res) => {
    const categoryID: number = Number(req.params.id)
    getCategory(categoryID)
        .then(category => res.status(200).json(category))
        .catch(() => res.status(400).json({ message: "Failed to fetch categories" }))
})

/**
 * Deletes category by name
 */
categoryRouter.delete('/:id', (req, res) => {
    const categoryID: number = Number(req.params.id)
    deleteCategory(categoryID)
        .then(categories => res.status(200).json(categories))
        .catch(() => res.status(400).json({ message: "Failed to delete category: "+categoryID+"!" }))
})


/**
 * Updates category name
 */
categoryRouter.patch('/:id', (req, res) => {
    const categoryID: number = Number(req.params.id)
    const newName: string = req.body.name
    updateCategory(categoryID,newName)
        .then(categories => res.status(200).json(categories))
        .catch(() => res.status(400).json({ message: "Failed to update category "+categoryID+ " to "+newName+"!" }))
})

export default categoryRouter;
