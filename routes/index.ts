import { Router } from "express";
import testRouter from "./test.routes";
import authRouter from "./auth.routes";
import gameRouter from "./game.routes";
import userRouter from "./user.routes";
import categoryRouter from "./category.routes";
import groupRouter from "./groupRoutes";

import { adminRouter } from "./admin";
import { playsRouter } from "./plays.routes";
import { chatRouter } from "./chat.routes";
import { groupMessageRouter } from "./groupMessage";
import { inviteRouter } from "./invites";
import { rentsRouter } from "./rents.routes";
import { reviewRouter } from "./review.routes";

const routes = Router();

routes.use("/test", testRouter);
routes.use("/auth", authRouter);
routes.use("/user", userRouter);
routes.use("/game", gameRouter);
routes.use("/category", categoryRouter);
routes.use("/group", groupRouter);

routes.use("/invites", inviteRouter);
routes.use("/chats", chatRouter);
routes.use("/groupMessages", groupMessageRouter);
routes.use("/plays", playsRouter);
routes.use("/admin", adminRouter);
routes.use("/rents", rentsRouter);
routes.use("/review", reviewRouter)

export default routes;
