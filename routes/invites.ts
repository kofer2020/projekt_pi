import { Router } from "express";
import { prisma } from "../services/database";
import { Request } from "../types";

export const inviteRouter = Router();

inviteRouter.get("/", (req: Request, res) => {
  prisma.invitation
    .findMany({ where: { player_id: req.userId }, include: {groups: true, users: true} })
    .then((inv) => res.json(inv))
    .catch((err) => res.status(500).json(err));
});

inviteRouter.get("/:id", (req: Request, res) => {
  prisma.invitation
    .findMany({ where: { group_id: parseInt(req.params.id) }, include: {users: true} })
    .then((inv) => res.json(inv))
    .catch((err) => res.status(500).json(err));
});

inviteRouter.post("/", async (req: Request, res) => {
  const { groupId, username } = req.body;

  const user = await prisma.users.findUnique({where: {username}})

  if(!user) {
    return res.status(404).json({error: 'cant find user'})
  }

  prisma.invitation
    .create({
      data: {
        expires_at: new Date(Date.now() + 1000 * 60 * 60 * 24),
        groups: { connect: { id: groupId } },
        users: { connect: { username } },
      },
    })
    .then(() => res.json({ ok: true }))
    .catch((err) => res.status(500).json(err));
});

inviteRouter.patch("/:groupId/accept", async (req: Request, res) => {
  try {
    const group_id = parseInt(req.params.groupId);
    const inviteWhere = { group_id, player_id: req.userId };

    const invite = await prisma.invitation.findFirst({ where: inviteWhere });

    if (!invite) {
      return res.status(404).json({ error: "not found" });
    }

    await prisma.members.create({
      data: {
        users: { connect: { id: req.userId } },
        groups: { connect: { id: group_id } },
      },
    });

    await prisma.invitation.deleteMany({ where: inviteWhere });

    res.json({ ok: true });
  } catch (err) {
    res.status(500).json(err);
  }
});
