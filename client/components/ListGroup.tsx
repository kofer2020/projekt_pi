import React, { useState } from "react";
import {
  Box,
  HStack,
  Heading,
  Button,
  IconButton,
  SimpleGrid,
  VStack,
  Text,
  Flex,
  useBoolean,
  Spinner,
  Tag,
} from "@chakra-ui/core";

import { DeleteIcon, EditIcon, InfoIcon, ArrowUpIcon } from "@chakra-ui/icons";
import { api, useAppContext } from "../pages/_app";
import { Link } from "chakra-next-link";

export const ListGroup = ({
  isNew = false,
  group = undefined,
  refetch = undefined,
}) => {
  const [editing, { on, off }] = useBoolean(isNew);
  const [info, { toggle, off: offInfo }] = useBoolean(false);
  const [request, { toggle: toggleReq, off: offReq }] = useBoolean(false);
  const [groupName, setGroupName] = useState(group?.name);

  const onSubmit = async () => {
    if (isNew) {
      setGroupName("");

      const res = await api(`/group`, {
        method: "POST",
        body: JSON.stringify({ name: group.name }),
      });
    } else {
      const res = await api(`/group/${group.id}`, {
        method: "PUT",
        body: JSON.stringify({ name: groupName }),
      });

      off();
    }

    await refetch();
  };

  const { data } = useAppContext();

  const isOwner = group.owner_id === data.user?.id;
  const isMember = !!group.members.find((m) => m.player_id === data.user?.id);

  return (
    <Flex
      as="form"
      onSubmit={(e) => {
        e.preventDefault();
        onSubmit();
      }}
      p="4"
      borderRadius="5px"
      w="60%"
      justifyContent="space-between"
      alignItems="flex-start"
      shadow="sm"
      borderWidth="1px"
    >
      <VStack spacing={3} align="flex-start">
        <Link href={`/group/${group.id}`}>
          <Heading size="md" color="blue.700">
            {group?.name}
          </Heading>
        </Link>
        {info && <Text> {group?.description}</Text>}
      </VStack>
      {request ? (
        <Button type="submit" onClick={toggleReq}>
          Cancel
        </Button>
      ) : (
        <HStack>
          <Tag colorScheme="orange">
            {group.capacity - group.members.length - 1}/{group.capacity}
          </Tag>
          <IconButton
            variant={info ? "solid" : "outline"}
            colorScheme="orange"
            aria-label="Info"
            onClick={() => {
              toggle();
              request && offReq();
            }}
            icon={<InfoIcon />}
          />
          {!(isMember || isOwner) && (
            <IconButton
              variant="outline"
              colorScheme="blue"
              aria-label="Invite request"
              onClick={() => {
                toggleReq();
                info && offInfo();
              }}
              icon={<ArrowUpIcon />}
            />
          )}
        </HStack>
      )}
    </Flex>
  );
};
