import * as React from "react";
import {
  VStack,
  FormControl,
  InputGroup,
  InputLeftElement,
  Input,
  Button,
  Heading,
  Avatar,
} from "@chakra-ui/core";
import { EmailIcon, LockIcon, AtSignIcon } from "@chakra-ui/icons";

interface UpdateForm {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
  companyName?: string;
  isCompany?: boolean;
}
const filterBySize = (file) => {
  //filter out images larger than 5MB
  return file.size <= 5242880;
};

interface toggleProps {
  edit: boolean;
  toggleEdit: () => void;
}

export const UpdateProfile: React.FC<toggleProps> = ({
  edit,
  toggleEdit,
  children,
}) => {
  const [loading, setLoading] = React.useState(false);
  const [image, setImage] = React.useState("");

  const uploadImage = async (e) => {
    const files = e.target.files;
    const data = new FormData();
    data.append("file", files[0]);
    data.append("upload_preset", "gameEx_user");
    setLoading(true);

    const res = await fetch(
      "	https://api.cloudinary.com/v1_1/gameex/image/upload",
      {
        method: "POST",
        body: data,
      }
    );

    const file = await res.json();
    console.log(file);

    setImage(file.secure_url);
    setLoading(false);
  };

  return (
    <>
      <VStack spacing="5" width="80%">
        <Heading size="lg" color="blue.700" left="-20px">
          Enter new data:
        </Heading>
        <FormControl>
          <InputGroup>
            <InputLeftElement
              pointerEvents="none"
              children={<AtSignIcon color="gray.300" size={100} />}
            />
            <Input
              name="username"
              type="username"
              placeholder="Username"
              isRequired
            />
          </InputGroup>
        </FormControl>

        <FormControl>
          <InputGroup>
            <InputLeftElement
              pointerEvents="none"
              children={<EmailIcon color="gray.300" />}
            />
            <Input name="email" type="email" placeholder="Email" isRequired />
          </InputGroup>
        </FormControl>

        <FormControl>
          <InputGroup>
            <InputLeftElement
              pointerEvents="none"
              children={<LockIcon color="gray.300" />}
            />
            <Input 
            name="password" 
            type="password"
            placeholder="Password" 
            isRequired />
          </InputGroup>
        </FormControl>
        <FormControl>
          <InputGroup>
            <InputLeftElement
              pointerEvents="none"
              children={<LockIcon color="gray.300" />}
            />
            <Input
              name="confirmPassword"
              type="password"
              placeholder="Confirm password"
              isRequired
            />
          </InputGroup>
        </FormControl>

        <Input
          type="file"
          name="file"
          placeholder="Upload an Image"
          onChange={uploadImage}
        />
        {loading && <h3> Loading ...</h3>}
        {image && <Avatar size="2xl" src={image} />}

        <Button
          type="submit"
          colorScheme="orange"
          top="10px"
          w="50%"
          onClick={toggleEdit}
        >
          Update
        </Button>
      </VStack>
    </>
  );
};
