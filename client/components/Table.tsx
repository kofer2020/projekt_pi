import styled from "@emotion/styled";

export const Table = styled.table`
  width: 100%;

  td,
  th {
    padding: 0.5em;
    font-size: 1.25em;
    word-break: break-all;
    text-align: left;
    border-bottom: 1px solid #adadad;
  }

  th {
    font-size: 1.5em;
    font-weight: 700;
  }
`;
