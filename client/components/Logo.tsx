import { Heading, HStack } from "@chakra-ui/core";
import React from "react";

export const Logo = () => {
  return (
    <HStack spacing="0">
      <Heading as="h1" size="2xl" color="blue.700">
        Game
      </Heading>
      <Heading as="h1" size="2xl" color="red.700">
        Ex
      </Heading>
    </HStack>
  );
};
