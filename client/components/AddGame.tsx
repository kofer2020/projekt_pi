import React from "react";
import {
  Heading,
  Box,
  Grid,
  GridItem,
  FormControl,
  FormLabel,
  Select,
  HStack,
  Button,
  Image,
  VStack,
  Spinner,
  Flex,
} from "@chakra-ui/core";
import Input from "./Input";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import { Game } from "../../interfaces/gamesTypes";
import useSWR from "swr";
import { api } from "../pages/_app";
import { LinkButton } from "chakra-next-link";

interface AddGameForm {
  name: string;
  description?: string;
  owner: string;
  photo_url?: string;
  maxPlayers: string;
  duration: string; //Minutes
  link?: string; //third-party link
  no_of_copies: string;
  category: string;
  price: number;
}

export const AddGame: React.FC = ({ children }) => {
  const [loading, setLoading] = React.useState(false);
  const [image, setImage] = React.useState("");

  const { handleSubmit, register, formState } = useForm<AddGameForm>();

  const router = useRouter();
  const { data } = useSWR("/category");

  const onSubmit = async (values: AddGameForm) => {
    const body: Omit<Game, "id"> = {
      name: values.name,
      description: values.description,
      owner: values.owner,
      photo_url: image,
      max_players: parseInt(values.maxPlayers),
      duration: parseInt(values.duration),
      link: values.link,
      no_of_copies: parseInt(values.no_of_copies),
      //@ts-ignore
      category: {
        id: parseInt(values.category),
      },
      // price: values.price,
      is_suggestion: true,
    };

    const res = await api(`/game`, {
      method: "POST",
      body: JSON.stringify(body),
    });

    router.push("/games");
  };

  const uploadImage = async (e) => {
    const files = e.target.files;
    const data = new FormData();
    data.append("file", files[0]);
    data.append("upload_preset", "gameEx_game");
    setLoading(true);

    const res = await fetch(
      "https://api.cloudinary.com/v1_1/gameex/image/upload",
      {
        method: "POST",
        body: data,
      }
    );

    const file = await res.json();
    console.log(file);

    setImage(file.secure_url);
    setLoading(false);
  };
  return (
    <>
      <Heading size="lg" mb="30px" color="orange.800">
        Add Game
      </Heading>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid
          templateRows={"repeat(9, 1fr)"}
          templateColumns={["repeat(1, 2fr)", "repeat(2, 2fr)"]}
          gap={2}
        >
          <GridItem rowSpan={1} colSpan={2}>
            <FormControl>
              <Input
                name="name"
                label="Game name"
                bg="gray.200"
                ref={register()}
              />
            </FormControl>
          </GridItem>
          <GridItem rowSpan={2} colSpan={2}>
            <FormControl>
              <Input
                as="textarea"
                // @ts-expect-error
                rows={4}
                name="description"
                label="Game description"
                bg="gray.200"
                ref={register()}
              />
            </FormControl>
          </GridItem>
          <GridItem colSpan={[2, 1]}>
            <FormControl>
              <Input
                type="number"
                size="md"
                name="maxPlayers"
                label="Max players"
                bg="gray.200"
                ref={register()}
              />
            </FormControl>
          </GridItem>
          <GridItem colSpan={[2, 1]}>
            <FormControl>
              <Input
                type="number"
                size="md"
                name="duration"
                label="Duration"
                bg="gray.200"
                ref={register()}
              />
            </FormControl>
          </GridItem>

          <GridItem colSpan={[2, 1]}>
            <FormControl>
              <Input
                type="number"
                size="md"
                name="no_of_copies"
                label="Number of copies"
                bg="gray.200"
                ref={register()}
              />
            </FormControl>
          </GridItem>
          <GridItem colSpan={[2, 1]}>
            <FormControl>
              <Input
                size="md"
                name="link"
                label="Link"
                bg="gray.200"
                ref={register()}
              />
            </FormControl>
          </GridItem>
          <GridItem colSpan={[2]}>
            <FormControl>
              <FormLabel>Category</FormLabel>
              {data === undefined ? (
                <Spinner />
              ) : (
                <Select
                  bg="gray.200"
                  name="category"
                  ref={register()}
                  isRequired
                >
                  {data.map((category) => (
                    <option value={category.id}>{category.name}</option>
                  ))}
                </Select>
              )}
            </FormControl>
          </GridItem>

          <GridItem colSpan={[2, 1]} rowSpan={2}>
            <FormControl>
              <Input
                type="file"
                name="file"
                placeholder="Upload an Image"
                onChange={uploadImage}
              />
            </FormControl>
          </GridItem>

          <GridItem colSpan={[2, 1]} rowSpan={2}>
            <Heading size="sm" color="blue.700">
              Upload image
            </Heading>
            {loading && <h3> Loading ...</h3>}
            {image && <Image size="2xl" src={image} />}
          </GridItem>

          <GridItem colSpan={2}>
            <Flex alignItems="flex-end" h="100%">
              <HStack justify="flex-end" w="100%">
                <LinkButton px="8" href="/games">
                  Cancel
                </LinkButton>
                <Button
                  type="submit"
                  bg="blue.600"
                  color="white"
                  px="10"
                  isLoading={formState.isSubmitting}
                >
                  Add
                </Button>
              </HStack>
            </Flex>
          </GridItem>
        </Grid>
      </form>
    </>
  );
};
