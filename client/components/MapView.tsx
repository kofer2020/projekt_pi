import { Heading } from "@chakra-ui/core";
import React, { useState } from "react";
import ReactMapGL, { Marker as MapMarker } from "react-map-gl";
import { api, useAppContext } from "../pages/_app";

const Marker = ({ marker, revalidateMarker, groupId }) => {
  if (!marker) {
    return null;
  }

  return (
    <MapMarker
      offsetTop={-48}
      offsetLeft={-24}
      latitude={marker.latitude}
      longitude={marker.longitude}
    >
      <img
        onContextMenu={async (e) => {
          e.preventDefault();
          await api(`/group/marker/${groupId}`, {
            method: "DELETE",
            body: JSON.stringify(marker),
          });
          await revalidateMarker();
        }}
        src="https://img.icons8.com/color/48/000000/marker.png"
      />
    </MapMarker>
  );
};

const Map = ({ isGroupOwner, groupId, revalidateMarker, marker }) => {
  const [mapViewport, setMapViewport] = useState({
    height: "300px",
    width: "400px",
    longitude: 15.970843975139271,
    latitude: 45.80113431099271,
    zoom: 15,
  });

  return (
    <ReactMapGL
      {...mapViewport}
      mapboxApiAccessToken="pk.eyJ1IjoiZXJuZWJ1dGEiLCJhIjoiY2s2bDVwYTRlMGFwdDNncGE0ZWdjaWZzMCJ9.2PppNmYQsYZ8HDSjEGwjCA"
      mapStyle="mapbox://styles/ernebuta/ck6l5q6me1dmn1ip74713pndm"
      onViewportChange={setMapViewport}
      onClick={async (x) => {
        if (x.srcEvent.which === 1 && isGroupOwner) {
          await api(`/group/marker/${groupId}`, {
            method: "POST",
            body: JSON.stringify({
              longitude: x.lngLat[0],
              latitude: x.lngLat[1],
            }),
          });
          await revalidateMarker();
        }
      }}
    >
      <Marker
        marker={marker}
        revalidateMarker={revalidateMarker}
        groupId={groupId}
      />
    </ReactMapGL>
  );
};

export const MapView = ({ marker, groupOwner, groupId, revalidateMarker }) => {
  const { data } = useAppContext();
  const isGroupOwner = data?.user?.id && data.user.id === groupOwner;

  return (
    <>
      {!marker && <Heading>No marker</Heading>}
      <Map
        marker={marker}
        isGroupOwner={isGroupOwner}
        groupId={groupId}
        revalidateMarker={revalidateMarker}
      />
    </>
  );
};
