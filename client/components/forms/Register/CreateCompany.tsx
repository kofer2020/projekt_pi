import React from "react";
import {
  Button,
  Flex,
  FormControl,
  Input,
  InputGroup,
  InputLeftElement,
  VStack,
} from "@chakra-ui/core";
import {
  InfoIcon,
  LinkIcon,
  PhoneIcon,
  TriangleDownIcon,
} from "@chakra-ui/icons";
import { useForm, UseFormMethods } from "react-hook-form";

export interface CreateCompanyType {
  name: string;
  web_address?: string;
  address: string;
  telephone: string;
}

export interface CreateCompanyFormProps {
  onSubmit: (
    values: CreateCompanyType,
    setError: UseFormMethods<CreateCompanyType>["setError"]
  ) => Promise<any>;
  goBack: (a?: { type: string; data: any }) => void;
  data: CreateCompanyType;
}

export const CreateCompanyForm: React.FC<CreateCompanyFormProps> = ({
  onSubmit,
  goBack,
  data,
}) => {
  const {
    handleSubmit,
    register,
    errors,
    setError,
    formState,
    getValues,
  } = useForm<CreateCompanyType>({ defaultValues: data });

  const submit = async (values: CreateCompanyType) => {
    await onSubmit(values, setError);
  };

  const fcProps = (name: string) => ({
    isInvalid: !!errors[name],
    isDisabled: formState.isSubmitting,
  });

  return (
    <VStack spacing="5" as="form" onSubmit={handleSubmit(submit)} w="100%">
      <FormControl {...fcProps("name")}>
        <InputGroup>
          <InputLeftElement pointerEvents="none">
            <InfoIcon color="gray.300" size={100} />
          </InputLeftElement>
          <Input
            ref={register}
            name="name"
            placeholder="Company name"
            isRequired
          />
        </InputGroup>
      </FormControl>

      <FormControl {...fcProps("web_address")}>
        <InputGroup>
          <InputLeftElement pointerEvents="none">
            <LinkIcon color="gray.300" />
          </InputLeftElement>
          <Input ref={register} name="web_address" placeholder="URL" />
        </InputGroup>
      </FormControl>

      <FormControl {...fcProps("address")}>
        <InputGroup>
          <InputLeftElement pointerEvents="none">
            <TriangleDownIcon color="gray.300" />
          </InputLeftElement>
          <Input
            ref={register}
            name="address"
            type="address"
            placeholder="Address"
            isRequired
          />
        </InputGroup>
      </FormControl>
      <FormControl {...fcProps("telephone")}>
        <InputGroup>
          <InputLeftElement pointerEvents="none">
            <PhoneIcon color="gray.300" />
          </InputLeftElement>
          <Input
            ref={register}
            name="telephone"
            placeholder="Phone number"
            isRequired
          />
        </InputGroup>
      </FormControl>

      <Flex justify="space-between" w="100%">
        <Button onClick={() => goBack({ type: "create", data: getValues() })}>
          Go back
        </Button>
        <Button
          type="submit"
          colorScheme="orange"
          isLoading={formState.isSubmitting}
          w="50%"
        >
          Create
        </Button>
      </Flex>
    </VStack>
  );
};
