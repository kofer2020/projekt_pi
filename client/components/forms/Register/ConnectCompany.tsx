import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Select,
  Spinner,
  VStack,
} from "@chakra-ui/core";
import React, { useState } from "react";
import useSWR from "swr";

export interface ConnectCompanyType {
  id: number;
}

interface ConnectCompanyProps {
  onSubmit: (values: ConnectCompanyType) => Promise<any>;
  goBack: (a?: { type: string; data: any }) => void;
  data?: ConnectCompanyType;
}

export const ConnectCompanyForm: React.FC<ConnectCompanyProps> = ({
  goBack,
  data,
  onSubmit,
}) => {
  const [selected, setSelected] = useState<number>(data?.id);
  const { data: companies, error: companiesError } = useSWR("/auth/companies");

  return (
    <VStack spacing="5" w="100%">
      <FormControl isLoading={!companies}>
        <FormLabel htmlFor="companies">Company</FormLabel>
        {!companies ? (
          <Spinner />
        ) : companiesError ? (
          <Heading>Error loading companies</Heading>
        ) : (
          <Select
            value={selected}
            onChange={(e) => setSelected(parseInt(e.target.value))}
          >
            {companies.map((company) => (
              <option value={company.id}>{company.name}</option>
            ))}
          </Select>
        )}
      </FormControl>
      <Flex justify="space-between" w="100%">
        <Button
          onClick={() => goBack({ type: "connect", data: { id: selected } })}
        >
          Go back
        </Button>
        <Button
          onClick={() => onSubmit({ id: selected })}
          colorScheme="orange"
          w="50%"
        >
          Create
        </Button>
      </Flex>
    </VStack>
  );
};
