import { VStack, Flex, Button } from "@chakra-ui/core";
import React from "react";

export const CompanyTypeForm = ({ choseCompanyType, goBack }) => {
  return (
    <VStack pt="4" spacing="10" align="flex-start">
      <Flex justify="space-between">
        <Button
          onClick={choseCompanyType(false)}
          w="45%"
          p="12"
          colorScheme="blue"
        >
          Connect to <br />
          existing company
        </Button>
        <Button
          onClick={choseCompanyType(true)}
          w="45%"
          p="12"
          colorScheme="green"
        >
          Create a <br />
          new company
        </Button>
      </Flex>
      <Button onClick={goBack}>Go back</Button>
    </VStack>
  );
};
