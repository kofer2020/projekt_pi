import {
  Button,
  FormControl,
  FormErrorMessage,
  Input,
  InputGroup,
  InputLeftElement,
  Switch,
  VStack,
} from "@chakra-ui/core";
import { AtSignIcon, EmailIcon, LockIcon } from "@chakra-ui/icons";
import React, { useEffect } from "react";
import { useForm, UseFormMethods } from "react-hook-form";

export interface RegisterFormType {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
}

export interface RegisterFormProps {
  isCompany: boolean;
  onSubmit: (
    values: RegisterFormType,
    setError: UseFormMethods<RegisterFormType>["setError"]
  ) => Promise<any>;
  data: RegisterFormType;
  errors?: { field: string; data: { message: string } };
}

export const RegisterForm: React.FC<RegisterFormProps> = ({
  onSubmit,
  isCompany,
  data,
  errors: passedErrors,
}) => {
  const {
    handleSubmit,
    register,
    errors,
    setError,
    formState,
    clearErrors,
  } = useForm<RegisterFormType>({ defaultValues: data });

  useEffect(() => {
    if (passedErrors) {
      console.log(passedErrors);
      setError(passedErrors.field as any, passedErrors.data);
    }
  }, [passedErrors]);

  const submit = async (values: RegisterFormType) => {
    clearErrors();

    if (values.password.length < 8) {
      return setError("password", { message: "Too short" });
    }

    if (values.password !== values.confirmPassword) {
      return setError("confirmPassword", { message: "Passwords don't match" });
    }

    await onSubmit(values, setError);
  };

  const fcProps = (name: string) => ({
    isInvalid: !!errors[name],
    isDisabled: formState.isSubmitting,
  });

  return (
    <VStack
      p="30px"
      spacing="5"
      as="form"
      onSubmit={handleSubmit(submit)}
      w="100%"
    >
      <FormControl {...fcProps("username")}>
        <InputGroup>
          <InputLeftElement pointerEvents="none">
            <AtSignIcon color="gray.300" size={100} />
          </InputLeftElement>
          <Input
            ref={register}
            name="username"
            type="username"
            placeholder="Username"
            isRequired
          />
        </InputGroup>
        {errors.username && (
          <FormErrorMessage>{errors.username.message}</FormErrorMessage>
        )}
      </FormControl>

      <FormControl {...fcProps("email")}>
        <InputGroup>
          <InputLeftElement pointerEvents="none">
            <EmailIcon color="gray.300" />
          </InputLeftElement>
          <Input
            ref={register}
            name="email"
            type="email"
            placeholder="Email"
            isRequired
          />
        </InputGroup>
        {errors.email && (
          <FormErrorMessage>{errors.email.message}</FormErrorMessage>
        )}
      </FormControl>

      <FormControl {...fcProps("password")}>
        <InputGroup>
          <InputLeftElement pointerEvents="none">
            <LockIcon color="gray.300" />
          </InputLeftElement>
          <Input
            ref={register}
            name="password"
            type="password"
            placeholder="Password"
            isRequired
          />
        </InputGroup>
        {errors.password && (
          <FormErrorMessage>{errors.password.message}</FormErrorMessage>
        )}
      </FormControl>
      <FormControl {...fcProps("confirmPassword")}>
        <InputGroup>
          <InputLeftElement pointerEvents="none">
            <LockIcon color="gray.300" />
          </InputLeftElement>
          <Input
            ref={register}
            name="confirmPassword"
            type="password"
            placeholder="Confirm password"
            isRequired
          />
        </InputGroup>
        {errors.confirmPassword && (
          <FormErrorMessage>{errors.confirmPassword.message}</FormErrorMessage>
        )}
      </FormControl>

      <Button
        type="submit"
        colorScheme="orange"
        isLoading={formState.isSubmitting}
        top="10px"
        w="50%"
      >
        {isCompany ? "Continue" : "Signup"}
      </Button>
    </VStack>
  );
};
