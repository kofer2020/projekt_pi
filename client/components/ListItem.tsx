import React, { useState } from "react";
import {
  Box,
  HStack,
  Heading,
  Button,
  IconButton,
  SimpleGrid,
  VStack,
  Input,
  Flex,
  useBoolean,
  Spinner,
} from "@chakra-ui/core";

import { DeleteIcon, EditIcon } from "@chakra-ui/icons";
import useSWR from "swr";
import { api } from "../pages/_app";

const ListItem = ({
  isNew = false,
  category = undefined,
  refetch = undefined,
}) => {
  const [editing, { on, off }] = useBoolean(isNew);
  const [categoryName, setCategoryName] = useState(category?.name);

  const onDelete = async () => {
    await api(`/category/${category.id}`, { method: "DELETE" });
    await refetch();
  };

  const onSubmit = async () => {
    if (isNew) {
      setCategoryName("");

      const res = await api(`/category`, {
        method: "POST",
        body: JSON.stringify({ name: categoryName }),
      });
    } else {
      const res = await api(`/category/${category.id}`, {
        method: "PUT",
        body: JSON.stringify({ name: categoryName }),
      });

      off();
    }

    await refetch();
  };

  return (
    <Flex
      as="form"
      onSubmit={(e) => {
        e.preventDefault();
        onSubmit();
      }}
      p="4"
      borderRadius="5px"
      w="90%"
      justifyContent="space-between"
      shadow="sm"
      borderWidth="1px"
    >
      {editing ? (
        <Input
          mr="4"
          onChange={(e) => setCategoryName(e.target.value)}
          value={categoryName}
        />
      ) : (
        <Heading size="md" color="blue.700">
          {category?.name}
        </Heading>
      )}
      {editing ? (
        <Button type="submit">Save</Button>
      ) : (
        <HStack>
          <IconButton
            variant="outline"
            colorScheme="red"
            aria-label="Delete category"
            onClick={onDelete}
            icon={<DeleteIcon />}
          />
          <IconButton
            variant="outline"
            colorScheme="green"
            aria-label="Edit category"
            onClick={on}
            icon={<EditIcon />}
          />
        </HStack>
      )}
    </Flex>
  );
};

export const Category: React.FC = ({ children }) => {
  const { data, error, revalidate } = useSWR("/category");

  if (!data) {
    return <Spinner />;
  }

  if (error) {
    return <Heading>Error</Heading>;
  }

  console.log(data);

  return (
    <VStack>
      <ListItem isNew refetch={revalidate} />
      {data.map((category) => (
        <ListItem category={category} refetch={revalidate} />
      ))}
    </VStack>
  );
};
