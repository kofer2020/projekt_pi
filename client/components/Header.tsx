import { Flex, VStack, Avatar, Button, HStack, Heading } from "@chakra-ui/core";
import React from "react";
import { Link, LinkButton } from "chakra-next-link";

import { Logo } from "./Logo";
import { useAppContext } from "../pages/_app";
import { useRouter } from "next/router";

export const Header: React.FC = ({ children }) => {
  const router = useRouter();
  const { data, setData } = useAppContext();

  const logout = async () => {
    localStorage.removeItem("token");
    setData({ user: undefined });
    router.push("/");
  };

  return (
    <Flex justify="space-between" w="100%">
      <Link href="/home">
        <Logo />
      </Link>

      <VStack>
        <HStack>
          {data?.user?.id && !data.user.is_accepted && (
            <Heading>NOT YET ACCEPTED</Heading>
          )}
          <LinkButton href="/games" colorScheme="purple">
            Games
          </LinkButton>
          <LinkButton href="/chat" colorScheme="blue">
            Chat
          </LinkButton>
          <Button onClick={logout} colorScheme="red">
            Logout
          </Button>
          <Link href="/profile">
            <Avatar
              size="md"
              bgColor="red.700"
              name={data.user?.username}
              src={data.user?.photourl}
            />
          </Link>
        </HStack>
        {children}
      </VStack>
    </Flex>
  );
};
