import React from "react";
import { Checkbox as ChakraCheckbox } from "@chakra-ui/core";
import { Controller } from "react-hook-form";

export const Checkbox: React.FC<{
  label: string;
  control: any;
  name: string;
  defaultValue?: boolean;
}> = ({ label, name, control, defaultValue }) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      render={({ value, ref, onChange }) => (
        <ChakraCheckbox
          ref={ref}
          onChange={(e) =>
            onChange({
              ...e,
              target: { ...e.target, value: e.target.checked },
            })
          }
          isChecked={value}
        >
          {label}
        </ChakraCheckbox>
      )}
    />
  );
};
