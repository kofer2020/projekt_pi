import * as React from "react";
import {
  Box,
  Image,
  Flex,
  Badge,
  Text,
  Icon,
  Heading,
  Link,
  HStack,
  Button,
  VStack,
} from "@chakra-ui/core";
import { StarIcon } from "@chakra-ui/icons";

interface CardProps {
  id: number;
  name: string;
  photo_url?: string;
  category: {
    id: number;
    name: string;
  };
  owner: string;
  description?: string;
  maxPlayers: string;
  duration: string; //Minutes
  link?: string; //third-party link
  no_of_copies: string;
  price: number;
  review: number;
  plays: { offer_to_rent: boolean; price: number }[];
}

export const formatPrice = (price: number) => {
  if (price === -1) {
    return `free`;
  }

  return `${price}kn`;
};

export const Card: React.FC<{ data: CardProps; open: any }> = ({
  data,
  open,
}) => {
  const [from, to] = data.plays
    .map((a) => a.price)
    .reduce(
      (val, curr) => {
        let [min, max] = val;

        if (min === undefined) {
          return [curr, curr];
        }

        if (curr < min) {
          return [curr, max];
        }

        return [min, curr];
      },
      [undefined, undefined]
    );

  const priceRange =
    data.plays.length === 0
      ? "Not in stock"
      : `Price: ${
          from === to
            ? `${formatPrice(from)}`
            : `${formatPrice(from)} - ${formatPrice(to)}`
        }`;

  return (
    <Flex
      p="5"
      maxW="320px"
      borderWidth="1px"
      boxShadow="lg"
      justifyContent="space-between"
      flexDir="column"
    >
      <VStack>
        <Heading size="lg" textAlign="center" mb="10px">
          {data.name}
        </Heading>
        <Image borderRadius="md" src={data.photo_url} />
        <Flex align="baseline" mt={2}>
          <Badge colorScheme="red">{data.category.name}</Badge>
          <Text
            ml={2}
            textTransform="uppercase"
            fontSize="sm"
            fontWeight="bold"
            color="blue.800"
          >
            Company &bull; {data.owner}
          </Text>
        </Flex>
        <Text mt={2} fontSize="xl" fontWeight="semibold" lineHeight="short">
          {data.description}
        </Text>
        <Link color="blue.600">{data.link}</Link>
      </VStack>

      <VStack align="flex-start" w="100%">
        <Heading size="md">{priceRange}</Heading>
        <HStack justify="space-between" mt="10px" align="center" w="100%">
          <Flex align="center">
            <StarIcon color="orange.400" h="10" fontSize="20px" />
            <Text ml={1} fontSize="md">
              <b>{data.review}</b> (190)
            </Text>
          </Flex>
          <Button
            p="2px 50px"
            colorScheme="green"
            onClick={open}
            isDisabled={(data as any).plays.length === 0}
          >
            Rent
          </Button>
        </HStack>
      </VStack>
    </Flex>
  );
};
