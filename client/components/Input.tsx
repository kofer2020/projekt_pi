import React, { forwardRef } from "react";
import {
  Input as ChakraInput,
  InputProps as ChakraInputProps,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormControlProps,
  Textarea,
} from "@chakra-ui/core";
import { ForwardRefRenderFunction } from "react";

const capitalize = (s: string) =>
  `${s.slice(0, 1).toUpperCase()}${s.slice(1).toLowerCase()}`;

interface InputProps extends ChakraInputProps {
  label?: string;
  error?: string;
  name: string;
  outerProps?: FormControlProps;
  noLabel?: boolean;
}

const resoleType = (name: string): string | undefined => {
  switch (name.toLowerCase()) {
    case "email":
      return "email";
    case "password":
      return "password";
    case "confirm password":
      return "confirm_password";
    default:
      return undefined;
  }
};

const Input: ForwardRefRenderFunction<HTMLInputElement, InputProps> = (
  props,
  ref
) => {
  const {
    name,
    isInvalid,
    noLabel,
    isRequired,
    error,
    as,
    label,
    placeholder,
    outerProps,
    ...rest
  } = props;

  let Component: any = ChakraInput;

  if (as === "textarea") {
    Component = Textarea;
  }

  return (
    <FormControl
      isInvalid={isInvalid || !!error}
      isRequired={isRequired}
      {...outerProps}
    >
      {!noLabel && (
        <FormLabel htmlFor={name}>{label || capitalize(name)}</FormLabel>
      )}
      <Component
        type={resoleType(name)}
        id={name}
        name={name}
        ref={ref}
        placeholder={placeholder}
        as={as}
        {...rest}
      />
      <FormErrorMessage>{error}</FormErrorMessage>
    </FormControl>
  );
};

export default forwardRef(Input);
