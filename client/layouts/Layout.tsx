import { Box } from "@chakra-ui/core";

export const Layout: React.FC = ({ children }) => {
  return (
    <>
      <Box
        pos="absolute"
        backgroundImage="url('https://cdn.hipwallpaper.com/i/92/28/6Ic2Ea.jpg')"
        h="100vh"
        w="100vw"
      />
      <Box
        pos="relative"
        width="100vw"
        height="100vh"
        d="flex"
        justifyContent="center"
        alignItems="center"
      >
        {children}
      </Box>
    </>
  );
};
