import { Box, Button } from "@chakra-ui/core";
import { useRouter } from "next/router";
import { Header } from "../components/Header";
import { useBgColor } from "../hooks/useBgColor";
import { Layout } from "./Layout";

export const UserLayout: React.FC = ({ children }) => {
  const bg = useBgColor();
  const router = useRouter();
  const logout = async () => {
    //logging out
    //send http request for cookie management

    localStorage.removeItem("token");
    router.push("/");
  };
  return (
    <Layout>
      <Box
        shadow="md"
        p="4"
        w="96%"
        h="94%"
        bg={bg}
        borderRadius="5px"
        overflowY="hidden"
      >
        <Header>{/* <Button onClick={logout}>Logout</Button> */}</Header>
        {children}
      </Box>
    </Layout>
  );
};
