import { Box, Heading, HStack, VStack } from "@chakra-ui/core";
import { LinkButton } from "chakra-next-link";
import React from "react";
import { ArrowBackIcon } from "@chakra-ui/icons";

export const AdminLayout: React.FC = ({ children }) => {
  return (
    <VStack w="100%">
      <HStack w="100%" p="4" bg="blue.800">
        <LinkButton href="/home">
          <ArrowBackIcon />
        </LinkButton>

        <Heading color="white">GameEx admin</Heading>
        <LinkButton href="/admin/users">Users</LinkButton>
        <LinkButton href="/admin/games">Games</LinkButton>
        <LinkButton href="/admin/categories">Categories</LinkButton>
      </HStack>
      <Box maxW="1200px" w="90%" shadow="md" p="8">
        {children}
      </Box>
    </VStack>
  );
};
