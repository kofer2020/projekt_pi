import { Box, Heading, VStack, HStack } from "@chakra-ui/core";
import { LinkButton } from "chakra-next-link";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useBgColor } from "../hooks/useBgColor";

import { useAppContext } from "./_app";

const Home: NextPage = () => {
  const bg = useBgColor();
  const router = useRouter();

  const { data } = useAppContext();
  if (data.user?.id) {
    router.push("/home");
  }

  return (
    <>
      <Box
        pos="absolute"
        backgroundImage="url('https://cdn.hipwallpaper.com/i/92/28/6Ic2Ea.jpg')"
        h="100vh"
        w="100vw"
      ></Box>
      <Box
        pos="relative"
        width="100vw"
        height="100vh"
        d="flex"
        justifyContent="center"
        alignItems="center"
      >
        <Box
          shadow="md"
          p="10"
          bg={bg}
          borderRadius="5px"
          flexDir="column"
          alignItems="center"
          spacing="24"
        >
          <VStack p={["20", "30", "40", "50"]} spacing="24">
            <Heading as="h1" fontSize={["4em", "6em"]} color="blue.700">
              Game
              <Box as="span" color="yellow.600">
                Ex
              </Box>
            </Heading>
            <Heading as="h2" size="lg" fontSize="40px" color="orange.600">
              Welcome to the world of board game
            </Heading>
            <LinkButton
              href="/auth/login"
              p="9"
              size="lg"
              transitionDuration="1s"
              colorScheme="orange"
            >
              Let's begin
            </LinkButton>
          </VStack>
        </Box>
      </Box>
    </>
  );
};

export default Home;
