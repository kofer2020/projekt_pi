import React, { useContext, useEffect, useState } from "react";
import { ChakraProvider } from "@chakra-ui/core";
import { AppProps } from "next/app";
import { SWRConfig } from "swr";

interface AppData {
  user?: {
    id: number;
    username: string;
    email: string;
    photourl: string;
    is_accepted: boolean;
    user_role: string;
    groups: {
      id: number;
      name: string;
    }[];
    members: {
      groups: {
        id: number;
        name: string;
      }[];
    }[];
  };
}
const AppProvider = React.createContext<{
  data: AppData;
  setData: React.Dispatch<React.SetStateAction<AppData>>;
  refetchAuth: () => Promise<any>;
}>({ data: {}, setData: () => {}, refetchAuth: () => Promise.resolve(null) });

export const useAppContext = () => useContext(AppProvider);

export const api = async (
  resource: string,
  init?: RequestInit,
  noJSON?: boolean
) => {
  const token =
    typeof window === "undefined"
      ? ""
      : window.localStorage.getItem("token") || "";

  const opts = {
    headers: {
      authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
      ...init?.headers,
    },
    ...init,
  };

  const res = await fetch(`${process.env.NEXT_PUBLIC_API}${resource}`, opts);

  if (noJSON) {
    return await res.text();
  }

  return await res.json();
};

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  const [data, setData] = useState<AppData>({});

  const refetchAuth = async () => {
    const token = window.localStorage.getItem("token");

    if (token) {
      api("/auth/me").then((user) => {
        if (user) {
          setData((d) => ({ ...d, user }));
        }
      });
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      refetchAuth();
    }
  }, []);

  return (
    <AppProvider.Provider value={{ data, setData, refetchAuth }}>
      <ChakraProvider resetCSS>
        <SWRConfig value={{ refreshInterval: 3000, fetcher: api }}>
          <Component {...pageProps} />
        </SWRConfig>
      </ChakraProvider>
    </AppProvider.Provider>
  );
};

export default MyApp;
