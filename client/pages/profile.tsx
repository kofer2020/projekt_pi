import {
  Box,
  Button,
  Heading,
  HStack,
  UnorderedList,
  Spinner,
  useBoolean,
  VStack,
  Checkbox as ChakraCheckbox,
  Flex,
  SimpleGrid,
  Avatar,
  Input,
  ListItem,
  useDisclosure,
  Modal,
  ModalBody,
  ModalHeader,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  FormControl,
  FormLabel,
} from "@chakra-ui/core";
import { NextPage } from "next";
import React, { useState } from "react";
import useSWR from "swr";

import { createPlay, removePlay, updatePlay } from "../lib/api/profile/private";
import { UserLayout } from "../layouts/UserLayout";
import { UpdateProfile } from "../components/UpdateProfile";
import { LinkButton } from "chakra-next-link";
import { api, useAppContext } from "./_app";
import { formatPrice } from "../components/Card";

const Play = ({ play, revalidate }) => {
  const [showEdit, { toggle }] = useBoolean();
  const [rentable, setRentable] = useState(play.offer_to_rent);
  const [count, setCount] = useState(play.count || 1);
  const [price, setPrice] = useState<number>(
    play.offer_to_rent ? play.price : undefined
  );

  return (
    <Box ml="8" bg="orange.200" p="3" borderRadius="5px" width="80%">
      <VStack align="flex-start">
        <Flex w="100%" justify="space-between">
          <Heading size="md" color="gray.800">
            {play.game.name} [{play.count}]
          </Heading>
          <HStack>
            {play.offer_to_rent && (
              <i>
                <b>Rentable</b> {formatPrice(play.price)}
              </i>
            )}
            <Button
              size="xs"
              onClick={() => removePlay(play.game_id, revalidate)}
              colorScheme="red"
            >
              Delete
            </Button>
            <Button
              size="xs"
              onClick={toggle}
              colorScheme="green"
              variant={showEdit ? "solid" : "outline"}
            >
              Update
            </Button>
          </HStack>
        </Flex>
        {showEdit && (
          <Box
            width="100%"
            bg="gray.100"
            p="4"
            borderBottomLeftRadius="5px"
            borderBottomRightRadius="5px"
          >
            <HStack>
              <ChakraCheckbox
                isChecked={rentable}
                onChange={(e) => setRentable(e.target.checked)}
                colorScheme="green"
                borderColor="gray.400"
              >
                Offer to rent
              </ChakraCheckbox>
              {rentable && (
                <HStack>
                  <Input
                    name="price"
                    type="number"
                    placeholder="Price"
                    value={price}
                    isRequired={rentable}
                    onChange={(e) => setPrice(parseInt(e.target.value))}
                  />
                  <Input
                    name="count"
                    type="number"
                    value={count}
                    isRequired={true}
                    onChange={(e) => setCount(parseInt(e.target.value))}
                  />
                  <Button
                    colorScheme="green"
                    size="sm"
                    variant={price === -1 ? "solid" : "outline"}
                    onClick={() => setPrice(-1)}
                  >
                    Free
                  </Button>
                </HStack>
              )}
              <Button
                size="sm"
                colorScheme="blue"
                onClick={async () => {
                  await updatePlay(
                    play.game_id,
                    rentable,
                    price,
                    count,
                    revalidate
                  );
                  toggle();
                  setPrice(rentable ? price : undefined);
                }}
              >
                Save
              </Button>
            </HStack>
          </Box>
        )}
      </VStack>
    </Box>
  );
};

const ViewData = ({ label: label, data: data }) => {
  return (
    <HStack>
      <Heading size="lg" color="blue.700">
        {label}:
      </Heading>
      <Heading size="lg" color="red.700">
        {data}
      </Heading>
    </HStack>
  );
};

const ProfilePage: NextPage = () => {
  const [search, setSearch] = useState("");
  const { data: dataProfile } = useAppContext();

  const { data, error, revalidate } = useSWR("/plays");
  const { data: games, error: gamesError } = useSWR(`/game?search=${search}`);
  const { data: invitations, revalidate: revalidateInvites } = useSWR(
    "/invites"
  );

  const modal = useDisclosure();
  const [edit, { toggle: toggleEdit }] = useBoolean(false);
  // const { handleSubmit, control, reset, register, watch } = useForm<PlayForm>();
  const [offersRent, setOffersRent] = useState<boolean>(false);
  const [gameId, setGameId] = useState<number>();
  const [price, setPrice] = useState<number>();
  const [count, setCount] = useState<number>(1);

  const isAdmin = dataProfile.user?.user_role === "Administrator";

  if (error) return <Heading>Error</Heading>;
  if (!data) return <Spinner />;

  const join = (id: number) => async () => {
    await api(`/invites/${id}/accept`, { method: "PATCH" });
    await revalidateInvites();
  };

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    await createPlay(
      { game_id: gameId, price, count, offer_to_rent: offersRent },
      revalidate
    );
    setSearch("");
    setPrice(undefined);
    setOffersRent(false);
    modal.onClose();
  };

  const filteredGames = (games || []).filter(
    (g) => !data.find((play) => play.game_id === g.id)
  );

  const isDisabled = !filteredGames.find((a) => a.id === gameId);

  return (
    <UserLayout>
      <Flex justify="center" mb="40px">
        <Heading color="red.700"> My playground </Heading>
      </Flex>
      <SimpleGrid columns={2} p="8">
        <VStack spacing={2}>
          <HStack mb="10px" spacing="6">
            <Heading size="lg">My games</Heading>
            <Button onClick={modal.onOpen} colorScheme="orange">
              Add new
            </Button>
          </HStack>

          <Modal {...modal}>
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>Add new game</ModalHeader>
              <ModalCloseButton />
              <ModalBody as={VStack} align="flex-start">
                <Input
                  value={search}
                  placeholder="search"
                  onChange={(e) => setSearch(e.target.value)}
                />
                {filteredGames.length === 0 && games && !gamesError ? (
                  <Heading>No games found</Heading>
                ) : (
                  <form onSubmit={onSubmit}>
                    <VStack spacing={4} align="flex-start">
                      {!gamesError && (
                        <Flex direction="column" width="100%">
                          {filteredGames.slice(0, 5).map((game) => (
                            <Box
                              p="2"
                              bg="gray.100"
                              borderRadius="5px"
                              m="2"
                              width="100%"
                              onClick={() => {
                                console.log("here");
                                setGameId(game.id);
                              }}
                            >
                              {game.name}
                              <Button
                                size="xs"
                                ml="2"
                                colorScheme="blue"
                                variant={
                                  gameId === game.id ? "solid" : "outline"
                                }
                                onClick={() => {
                                  console.log("here");
                                  setGameId(game.id);
                                }}
                              >
                                select
                              </Button>
                            </Box>
                          ))}
                          {filteredGames.length > 5 && <Box>...</Box>}
                        </Flex>
                      )}
                      <ChakraCheckbox
                        isChecked={offersRent}
                        onChange={(e) => setOffersRent(e.target.checked)}
                      >
                        Offer to rent
                      </ChakraCheckbox>
                      {offersRent && (
                        <Input
                          isRequired={offersRent}
                          type="number"
                          name="price"
                          value={price}
                          onChange={(e) => setPrice(parseInt(e.target.value))}
                          placeholder="Price"
                        />
                      )}
                      <FormControl>
                        <FormLabel>Quantity</FormLabel>
                        <Input
                          isRequired={true}
                          type="number"
                          name="count"
                          value={count}
                          onChange={(e) => setCount(parseInt(e.target.value))}
                          placeholder="Count"
                        />
                      </FormControl>
                      <Button
                        colorScheme="blue"
                        type="submit"
                        p="4px 38px"
                        isDisabled={isDisabled}
                      >
                        Add
                      </Button>
                    </VStack>
                  </form>
                )}
              </ModalBody>
            </ModalContent>
          </Modal>

          {data.map((play) => (
            <Play play={play} revalidate={revalidate} />
          ))}

          <Heading pt="40px">Invites:</Heading>
          {invitations ? (
            invitations.length === 0 ? (
              <Heading size="lg">No pending invites</Heading>
            ) : (
              <VStack w="50%">
                {invitations.map((inv) => (
                  <Flex
                    spacing={10}
                    bg="orange.100"
                    px="8"
                    py="2"
                    w="100%"
                    borderRadius="5px"
                    justify="space-between"
                    alignItems="center"
                  >
                    <Heading size="md">{inv.groups.name}</Heading>
                    <Button onClick={join(inv.groups.id)}>Join</Button>
                  </Flex>
                ))}
              </VStack>
            )
          ) : (
            <Spinner />
          )}
        </VStack>
        <Flex width="100%" justify="center">
          {edit ? (
            <UpdateProfile edit={edit} toggleEdit={toggleEdit} />
          ) : (
            <Box>
              <VStack spacing="6">
                <Avatar
                  name={dataProfile.user?.username}
                  src={dataProfile.user?.photourl}
                  width="160px"
                  height="160px"
                  border-radius="50%"
                />

                <Flex direction="column" mt="6%">
                  <ViewData
                    label="Username"
                    data={dataProfile.user?.username}
                  />
                  <ViewData label="Email" data={dataProfile.user?.email} />
                  <ViewData label="Role" data={dataProfile.user?.user_role} />
                </Flex>
                {isAdmin ? (
                  <HStack>
                    <LinkButton href="/admin/users">Admin</LinkButton>
                    <Button
                      colorScheme="blue"
                      p="10px 30px"
                      onClick={toggleEdit}
                    >
                      Edit
                    </Button>
                  </HStack>
                ) : (
                  <Button colorScheme="blue" p="10px 30px" onClick={toggleEdit}>
                    Edit
                  </Button>
                )}
              </VStack>
            </Box>
          )}
        </Flex>
      </SimpleGrid>
    </UserLayout>
  );
};

export default ProfilePage;
