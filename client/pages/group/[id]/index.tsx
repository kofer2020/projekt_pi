import React, { useState } from "react";
import { NextPage } from "next";
import {
  Box,
  Heading,
  SimpleGrid,
  HStack,
  VStack,
  IconButton,
  Flex,
  Text,
  Spinner,
  Button,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
} from "@chakra-ui/core";
import { UserLayout } from "../../../layouts/UserLayout";
import { DeleteIcon, CopyIcon } from "@chakra-ui/icons";
import { useRouter } from "next/router";
import { getId } from "../../../helpers/getId";
import useSWR from "swr";
import { api } from "../../_app";

import Input from "../../../components/Input";
import { MapView } from "../../../components/MapView";
import { Calendar, momentLocalizer, Views } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from "moment";
import * as dates from "../../../components/dates";
const localizer = momentLocalizer(moment);

const GroupPage: NextPage = () => {
  const router = useRouter();
  const id = getId(router.query);
  const { data, revalidate } = useSWR(id && `/group/${id}`);
  const {
    data: invites,
    error: invitesError,
    revalidate: revalidateInvites,
  } = useSWR(id && `/invites/${id}`);
  const { data: makerData, revalidate: revalidateMarker } = useSWR(
    id && `/group/marker/${id}`
  );

  const [username, setUsername] = useState<string>();

  const inviteUser = async () => {
    const res = await api(`/invites`, {
      method: "POST",
      body: JSON.stringify({ username, groupId: id }),
    });

    console.log(res);

    await revalidateInvites();
  };

  const { isOpen, onOpen, onClose } = useDisclosure();

  const removeUser = (userId: number) => async () => {
    await api(`/group/${id}/${userId}`, { method: "DELETE" });
    await revalidate();
  };

  const ColoredDateCellWrapper = ({ children }) =>
    React.cloneElement(React.Children.only(children), {
      style: {
        backgroundColor: "lightblue",
      },
    });

  let allViews = Object.keys(Views).map((k) => Views[k]);

  const handleSelect = ({ start, end }) => {
    const title = window.prompt("New Event name");
    if (title)
      //@ts-ignore
      this.setState({
        events: [
          //@ts-ignore
          ...this.state.events,
          {
            start,
            end,
            title,
          },
        ],
      });
  };

  return (
    <UserLayout>
      {data ? (
        <VStack w="100%">
          <Heading
            display="flex"
            justifyContent="center"
            color="red.800"
            pb="8"
          >
            {data.name}
          </Heading>
          <Button w="16%" p={6} colorScheme="orange" onClick={onOpen}>
            Calendar
          </Button>

          <Modal isOpen={isOpen} onClose={onClose} size={"full"}>
            <ModalOverlay />
            <ModalContent>
              <ModalCloseButton />
              <ModalBody align="flex-start">
                <Calendar
                  selectable
                  style={{ width: "90%", margin: "10px auto" }}
                  events={[]}
                  views={allViews}
                  step={60}
                  showMultiDayTimes
                  max={dates.add(dates.endOf(new Date(), "day"), -1, "hours")}
                  defaultDate={new Date()}
                  components={{
                    timeSlotWrapper: ColoredDateCellWrapper,
                  }}
                  localizer={localizer}
                  onSelectEvent={(event) => alert(event.title)}
                  onSelectSlot={handleSelect}
                  //dayLayoutAlgorithm={this.state.dayLayoutAlgorithm}
                />
              </ModalBody>
            </ModalContent>
          </Modal>

          <SimpleGrid columns={2} w="100%">
            <VStack spacing={6}>
              <Heading color="blue.800" size="lg">
                Members
              </Heading>

              <Flex
                spacing={10}
                bg="blue.100"
                p="8px"
                borderRadius="5px"
                justify="space-between"
                w="50%"
                alignItems="center"
              >
                <Heading size="md">{data.owner?.username}</Heading>
              </Flex>

              {data.members?.map((member) => (
                <Flex
                  spacing={10}
                  bg="orange.100"
                  p="8px"
                  borderRadius="5px"
                  justify="space-between"
                  w="50%"
                  alignItems="center"
                >
                  <Heading size="md">{member.users.username}</Heading>
                  <IconButton
                    variant="outline"
                    colorScheme="red"
                    aria-label="Delete user"
                    onClick={removeUser(member.users.id)}
                    icon={<DeleteIcon />}
                  />
                </Flex>
              ))}
            </VStack>

            <VStack spacing={6}>
              <Heading color="blue.800" size="lg">
                Invites
              </Heading>

              <HStack align="flex-end">
                <Input
                  name="username"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                />
                <Button onClick={inviteUser}>Invite</Button>
              </HStack>

              {invites ? (
                invites?.map((member) => (
                  <Flex
                    spacing={10}
                    bg="orange.100"
                    p="8px"
                    borderRadius="5px"
                    justify="space-between"
                    w="50%"
                    alignItems="center"
                  >
                    <Heading size="md">{member.users.username}</Heading>
                  </Flex>
                ))
              ) : (
                <Spinner />
              )}

              <MapView
                groupId={data.id}
                groupOwner={data.owner_id}
                marker={makerData || undefined}
                revalidateMarker={revalidateMarker}
              />
            </VStack>
          </SimpleGrid>
        </VStack>
      ) : (
        <Spinner />
      )}
    </UserLayout>
  );
};

export default GroupPage;
