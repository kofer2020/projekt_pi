import React from "react";
import { NextPage } from "next";
import { useBgColor } from "../../hooks/useBgColor";
import {
  Box,
  Heading,
  Grid,
  GridItem,
  FormControl,
  HStack,
  Button,
  FormLabel,
  Spinner,
  Select,
} from "@chakra-ui/core";
import { AddGame } from "../../components/AddGame";
import { users } from "@prisma/client";
import { useForm } from "react-hook-form";
import { Group } from "../../../interfaces/groupsTypes";
import Input from "../../components/Input";

import useSWR from "swr";
import { group } from "console";
import { UserLayout } from "../../layouts/UserLayout";
import { LinkButton } from "chakra-next-link";
import { useRouter } from "next/router";
import { api, useAppContext } from "../_app";

interface CreateGroupForm {
  capacity: string;
  name: string;
  description?: string;
  game: string;
}

const CreateGroup: NextPage = () => {
  const [loading, setLoading] = React.useState(false);
  const bg = useBgColor();
  const router = useRouter();

  const goBack = () => {
    router.push("/home");
  }
  const {
    handleSubmit,
    register,
    formState,
    setValue,
    watch,
  } = useForm<CreateGroupForm>();
  const cat = watch("group");

  const { data } = useAppContext();
  const { data: gameData } = useSWR("/game");

  const onSubmit = async (values: CreateGroupForm) => {
    const body = {
      capacity: parseInt(values.capacity),
      name: values.name,
      owner_id: data.user.id,
      description: values.description,
      game: {
        id: parseInt(values.game),
      },
    };

    const res = await api("/group", {
      method: "POST",
      body: JSON.stringify(body),
    });

    console.log(res);

    router.push("/home");
    // alert("New game is added");
    return;
  };

  return (
    <UserLayout>
      <Box
        overflowY="auto"
        maxH="80vh"
        width="90%"
        maxW="800px"
        p="6"
        shadow="md"
        bg="white"
        borderRadius="5px"
        m="40px auto"
      >
        <Heading size="lg" mb="30px" color="orange.800">
          Create group
        </Heading>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Grid
            templateRows="repeat(4, 1fr)"
            templateColumns="repeat(2, 1fr)"
            gap={2}
          >
            <GridItem rowSpan={1} colSpan={2}>
              <FormControl>
                <Input
                  name="name"
                  label="Group name"
                  bg="gray.200"
                  ref={register()}
                />
              </FormControl>
            </GridItem>
            <GridItem rowSpan={2} colSpan={2}>
              <FormControl>
                <Input
                  as="textarea"
                  name="description"
                  label="Group description"
                  bg="gray.200"
                  //@ts-ignore
                  rows={4}
                  ref={register()}
                />
              </FormControl>
            </GridItem>
            <GridItem colSpan={[2, 1]}>
              <FormControl>
                <Input
                  type="number"
                  size="md"
                  name="capacity"
                  label="Max members"
                  bg="gray.200"
                  ref={register()}
                />
              </FormControl>
            </GridItem>

            <GridItem colSpan={[2, 1]}>
              <HStack justify="flex-end" w="100%" mt="30px">
                <Button px="8"
                onClick={goBack}
                >Cancel</Button>
                <Button
                  type="submit"
                  bg="blue.600"
                  color="white"
                  px="10"
                  isLoading={formState.isSubmitting}
                >
                  Create
                </Button>
              </HStack>
            </GridItem>
            <GridItem>
              <FormControl>
                <FormLabel>Game</FormLabel>
                {gameData === undefined ? (
                  <Spinner />
                ) : (
                  <Select bg="gray.200" name="game" ref={register()} isRequired>
                    {gameData.map((game) => (
                      <option value={game.id}>{game.name}</option>
                    ))}
                  </Select>
                )}
              </FormControl>
            </GridItem>
          </Grid>
        </form>
      </Box>
    </UserLayout>
  );
};

export default CreateGroup;
