import React from "react";
import {
  Box,
  Heading,
  Input,
  InputGroup,
  Button,
  Text,
  VStack,
  InputLeftElement,
  HStack,
  FormControl,
  Flex,
  useColorModeValue,
} from "@chakra-ui/core";
import { NextPage } from "next";
import { EmailIcon, LockIcon } from "@chakra-ui/icons";
import { Link } from "chakra-next-link";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";

import { useBgColor } from "../../hooks/useBgColor";
import { LoginType } from "../../../interfaces/authorizationTypes";
import { Layout } from "../../layouts/Layout";
import { useAppContext } from "../_app";

interface LoginForm {
  email: string;
  password: string;
}

const login: NextPage = () => {
  const router = useRouter();
  const { data, setData, refetchAuth } = useAppContext();
  const inputColor = useColorModeValue("blue.800", "white");

  const { handleSubmit, errors, register, formState } = useForm<LoginForm>();
  const bg = useBgColor();

  if (data.user?.id) {
    router.push("/home");
  }

  const onSubmit = async (values: LoginForm) => {
    const body: LoginType = {
      email: values.email,
      password: values.password,
    };

    console.log("submit");
    const res = await fetch(`${process.env.NEXT_PUBLIC_API}/auth/login`, {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "content-type": "application/JSON",
      },
    });

    if (!res.ok) {
      const out = JSON.parse(await res.text());
      alert(out.message);
      return;
    }

    const { token, user } = await res.json();

    window.localStorage.setItem("token", token);
    await refetchAuth();

    router.push("/home");
  };

  return (
    <Layout>
      <Box shadow="md" p="10" w="80%" maxW="400px" bg={bg} borderRadius="5px">
        <VStack spacing="0">
          <HStack spacing="0">
            <Heading as="h1" size="2xl" color="blue.700">
              Game
            </Heading>
            <Heading as="h1" size="2xl" color="red.700">
              Ex
            </Heading>
          </HStack>

          <Heading as="h2" size="xl" color="orange.600">
            Login
          </Heading>
          <form onSubmit={handleSubmit(onSubmit)}>
            <VStack p="30px" spacing="5">
              <FormControl
                as={Flex}
                isInvalid={!!errors.email}
                isDisabled={formState.isSubmitting}
              >
                <InputGroup>
                  <InputLeftElement
                    pointerEvents="none"
                    children={<EmailIcon color="gray.300" />}
                  />
                  <Input
                    ref={register}
                    name="email"
                    type="email"
                    placeholder="Email"
                    color={inputColor}
                    w="15rem"
                    isRequired
                  />
                </InputGroup>
              </FormControl>

              <FormControl
                isInvalid={!!errors.password}
                isDisabled={formState.isSubmitting}
              >
                <InputGroup>
                  <InputLeftElement
                    pointerEvents="none"
                    children={<LockIcon color="gray.300" />}
                  />
                  <Input
                    ref={register}
                    name="password"
                    type="password"
                    placeholder="Password"
                    color={inputColor}
                    w="15rem"
                    isRequired
                  />
                </InputGroup>
              </FormControl>

              <Button
                type="submit"
                colorScheme="orange"
                top="10px"
                w="50%"
                isLoading={formState.isSubmitting}
              >
                Login
              </Button>
            </VStack>
          </form>
          <VStack>
            <Text color="blue.500">Not a member yet?</Text>
            <Link href="/auth/register">Register here</Link>
          </VStack>
        </VStack>
      </Box>
    </Layout>
  );
};

export default login;
