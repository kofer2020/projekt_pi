import React, { useState } from "react";
import {
  Box,
  Heading,
  VStack,
  Flex,
  Text,
  FormLabel,
  Switch,
  FormControl,
} from "@chakra-ui/core";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { Link } from "chakra-next-link";

import { RegistrationType } from "../../../interfaces/authorizationTypes";
import { useBgColor } from "../../hooks/useBgColor";
import { Layout } from "../../layouts/Layout";
import { Logo } from "../../components/Logo";
import {
  RegisterForm,
  RegisterFormProps,
  RegisterFormType,
} from "../../components/forms/Register/RegisterForm";
import { CompanyTypeForm } from "../../components/forms/Register/CompanyTypeForm";
import {
  CreateCompanyForm,
  CreateCompanyType,
} from "../../components/forms/Register/CreateCompany";
import {
  ConnectCompanyForm,
  ConnectCompanyType,
} from "../../components/forms/Register/ConnectCompany";
import { useGoHomeIfLoggedIn } from "../../hooks/useGoHomeIfLoggedIn";
import { useAppContext } from "../_app";

const UNIQ_ERROR = "Unique constraint failed on the fields: (`username`)";
const UNIQ_ERROR_EMAIL = "Unique constraint failed on the fields: (`email`)";

type OnSubmit = Parameters<RegisterFormProps["onSubmit"]>;

const companyDataToApi = (
  cd: CreateCompanyType | ConnectCompanyType
):
  | { companyId: number }
  | Pick<
      RegistrationType,
      "companyAddress" | "companyName" | "companyWebsite" | "companyTelephone"
    > => {
  if ((cd as any).id) {
    const connect = cd as ConnectCompanyType;

    return { companyId: connect.id };
  }

  const create = cd as CreateCompanyType;

  return {
    companyAddress: create.address,
    companyName: create.name,
    companyTelephone: create.telephone,
    companyWebsite: create.web_address,
  };
};

const createUser = async (
  userData: RegisterFormType,
  companyData: CreateCompanyType | ConnectCompanyType | undefined,
  setData: any
): Promise<{
  error:
    | { field: keyof RegisterFormType; data: { message: string } }
    | { field?: undefined; data: string }
    | null;
}> => {
  const body: RegistrationType = {
    ...userData,
    isCompany: !!companyData,
    ...(companyData && companyDataToApi(companyData)),
  };

  const res = await fetch(`${process.env.NEXT_PUBLIC_API}/auth/signup`, {
    method: "POST",
    body: JSON.stringify(body),
    headers: {
      "content-type": "application/JSON",
    },
  });
  if (!res.ok) {
    const out = await res.text();
    if (out.includes(UNIQ_ERROR)) {
      return {
        error: { field: "username", data: { message: "Already in use" } },
      };
    }

    if (out.includes(UNIQ_ERROR_EMAIL)) {
      return {
        error: { field: "email", data: { message: "Already in use" } },
      };
    }

    return { error: { data: out } };
  }
  const { token, user } = await res.json();

  // window.localStorage.setItem("token", token);
  // setData((d) => ({ ...d, user }));

  return { error: null };
};

const RegisterPage: NextPage = () => {
  const router = useRouter();
  const [isCompany, setIsCompany] = useState<boolean>(false);
  const [createNewCompany, setCreateNewCompany] = useState<boolean>();

  const [companyCreateData, setCreateData] = useState<CreateCompanyType>();
  const [companyConnectData, setConnectData] = useState<ConnectCompanyType>();
  const [userData, setUserData] = useState<RegisterFormType>();
  const [step, setStep] = useState(0);

  const [errors, setErrors] = useState<{
    field: string;
    data: { message: string };
  }>();

  const bg = useBgColor();

  const { data, setData } = useAppContext();
  if (data.user?.id) {
    router.push("/home");
  }

  const onSubmitInitial = async (
    ...[values, setError]: Parameters<RegisterFormProps["onSubmit"]>
  ) => {
    if (step === 0) {
      if (isCompany) {
        setUserData(values);
        return setStep(1);
      }

      const { error } = await createUser(values, undefined, setData);

      if (error) {
        if (error.field) {
          setStep(0);
          setErrors(errors);
        } else {
          alert(error.data);
        }

        return;
      }

      alert("Done");
      router.push("/auth/login");
    }
  };

  const onSubmitCreateCompany = async (values: CreateCompanyType) => {
    setCreateData(values);
    const { error } = await createUser(userData, values, setData);

    if (error) {
      if (error.field) {
        setErrors(error as any);
        setStep(0);
      } else {
        alert(error.data);
      }

      return;
    }

    router.push("/auth/login");
  };

  const onSubmitConnectCompany = async (values: ConnectCompanyType) => {
    setConnectData(values);
    const { error } = await createUser(userData, values, setData);

    if (error) {
      if (error.field) {
        setErrors(error as any);
        setStep(0);
      } else {
        alert(error.data);
      }

      return;
    }

    router.push("/auth/login");
  };

  const choseCompanyType = (option: boolean) => () => {
    setCreateNewCompany(option);
    setStep(2);
  };

  const goBack = (save?: { type: string; data: any }) => {
    if (save) {
      if (save.type === "connect") {
        save.data && setConnectData(save.data);
      } else {
        save.data && setCreateData(save.data);
      }
    }

    setStep((s) => s - 1);
  };

  return (
    <Layout>
      <Box shadow="md" p="10" w="90%" maxW="500px" bg={bg} borderRadius="5px">
        <VStack spacing="2">
          <Logo />
          <Heading as="h2" size="xl" color="orange.600">
            Signup
          </Heading>

          {step === 0 && (
            <FormControl as={Flex} justifyContent="center" alignItems="center">
              <FormLabel htmlFor="company" mb="0" mr="2">
                Are you a company?
              </FormLabel>
              <Switch
                id="company"
                isChecked={isCompany}
                onChange={(event) => setIsCompany(event.target.checked)}
              />
            </FormControl>
          )}

          {step === 0 ? (
            <RegisterForm
              errors={errors}
              isCompany={isCompany}
              onSubmit={onSubmitInitial}
              data={userData}
            />
          ) : step === 1 ? (
            <CompanyTypeForm
              choseCompanyType={choseCompanyType}
              goBack={goBack}
            />
          ) : step === 2 ? (
            createNewCompany ? (
              <CreateCompanyForm
                onSubmit={onSubmitCreateCompany}
                data={companyCreateData}
                goBack={goBack}
              />
            ) : (
              <ConnectCompanyForm
                onSubmit={onSubmitConnectCompany}
                data={companyConnectData}
                goBack={goBack}
              />
            )
          ) : null}

          <VStack>
            <Text color="blue.500">Already a member?</Text>
            <Link href="/auth/login">Login here</Link>
          </VStack>
        </VStack>
      </Box>
    </Layout>
  );
};

export default RegisterPage;
