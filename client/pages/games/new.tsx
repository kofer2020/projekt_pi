import React from "react";
import { NextPage } from "next";
import { Box } from "@chakra-ui/core";
import { useBgColor } from "../../hooks/useBgColor";
import { UserLayout } from "../../layouts/UserLayout";
import { AddGame } from "../../components/AddGame";

const AddGames: NextPage = () => {
  return (
    <UserLayout>
      <Box overflowY="auto" width="90%" bg="white" m="0 auto" p={[0, 6]}>
        <AddGame />
      </Box>
    </UserLayout>
  );
};

export default AddGames;
