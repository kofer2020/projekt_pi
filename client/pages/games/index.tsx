import React, { useState } from "react";
import { NextPage } from "next";
import {
  Box,
  Heading,
  VStack,
  Spinner,
  SimpleGrid,
  Flex,
  Modal,
  ModalBody,
  useDisclosure,
  Button,
  UseDisclosureReturn,
  ModalContent,
  ModalCloseButton,
  ModalHeader,
  ModalOverlay,
  ModalFooter,
} from "@chakra-ui/core";
import useSWR from "swr";
import { Card as GameCard, formatPrice } from "../../components/Card";
import { UserLayout } from "../../layouts/UserLayout";
import { AddIcon } from "@chakra-ui/icons";
import { Link, LinkIconButton } from "chakra-next-link";
import { api, useAppContext } from "../_app";
import PaypalExpressBtn from "react-paypal-express-checkout";

const client = {
  sandbox:
    "AXSNoMd8P2h-AhmcfxfnqbgxaqOhDMUlRgqAD314n123BuHg8l3Qre60RBB2Fr0mzUw8cgtpBj35lbAh",
};

const GameModal: React.FC<{
  modalProps: UseDisclosureReturn;
  data: any;
}> = ({ modalProps, data }) => {
  const {
    data: { user },
  } = useAppContext();

  const onSuccess = (play) => async (payment) => {
    console.log("The payment has succeeded!", payment);
    // await zapisi rent

    await api(`/plays`, {
      method: "PATCH",
      body: JSON.stringify({
        player_id: play.player_id,
        gameId: play.game_id,
        offerToRent: play.count > 1,
        count: play.count - 1,
      }),
    });

    const rent = {
      playsGame_id: play.game_id,
      playsPlayer_id: play.player_id,
      renter_id: user.id,
      player_id: play.player_id,
      price: play.price,
      transaction_id: payment.paymentID,
      when_ts: null,
      expires_at: null,
      processed_at: null,
    };

    await api(`/rents`, {
      method: "POST",
      body: JSON.stringify({ rent }),
    });
  };

  return (
    <Modal {...modalProps}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Modal Title</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          {data === undefined ? (
            <Spinner />
          ) : (
            <Flex direction="column">
              {data.plays.map((play) => (
                <Box p="4" shadow="md">
                  <Flex width="100%" justify="space-between">
                    <Box>
                      <Heading size="md">
                        Owner:
                        <Link href={`/user/${play.player_id}`}>
                          {play.users.username}
                        </Link>
                      </Heading>
                      <Heading size="md">Quantity: {play.count}</Heading>
                      {play?.offer_to_rent ? (
                        <Heading size="md">
                          Price: {formatPrice(play?.price)}{" "}
                        </Heading>
                      ) : (
                        <Heading size="md">Not for rent</Heading>
                      )}
                    </Box>
                    {play?.offer_to_rent &&
                      (user?.id === play.player_id ? (
                        "This is your ad"
                      ) : (
                        <PaypalExpressBtn
                          env="sandbox"
                          client={client}
                          currency="USD"
                          total={Number((play.price / 6.2).toFixed(2))}
                          onError={alert}
                          onSuccess={onSuccess(play)}
                        />
                      ))}
                  </Flex>
                </Box>
              ))}
            </Flex>
          )}
        </ModalBody>
        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={modalProps.onClose}>
            Close
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

const Games: NextPage = () => {
  const { data } = useSWR("/game");
  const modalProps = useDisclosure();

  const [selectedId, setSelectedId] = useState(-1);

  const openModal = (id: number) => () => {
    setSelectedId(id);
    modalProps.onOpen();
  };

  return (
    <UserLayout>
      <Box px="10" mt="10">
        <Heading position="sticky" color="red.700">
          Game store
        </Heading>

        <VStack mt="20px" align="flex-start">
          <SimpleGrid columns={3} spacing="10">
            {data === undefined ? (
              <Spinner />
            ) : (
              <>
                <GameModal
                  data={data.find((game) => game.id === selectedId)}
                  modalProps={modalProps}
                />
                {data.map((game) => (
                  <GameCard data={game} open={openModal(game.id)} />
                ))}
                <Flex
                  p="5"
                  maxW="320px"
                  borderWidth="1px"
                  boxShadow="lg"
                  justifyContent="center"
                  alignItems="center"
                >
                  <VStack spacing="10">
                    <Heading textAlign="center" size="lg" w="80%">
                      Suggest new game
                    </Heading>

                    <LinkIconButton
                      href="/games/new"
                      icon={<AddIcon />}
                      aria-label="Suggest new game"
                      w="20"
                      h="20"
                      borderRadius="50%"
                      colorScheme="green"
                    />
                  </VStack>
                </Flex>
              </>
            )}
          </SimpleGrid>
        </VStack>
      </Box>
    </UserLayout>
  );
};

export default Games;
