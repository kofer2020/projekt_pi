import {
  Button,
  Heading,
  HStack,
  ListItem,
  Spinner,
  UnorderedList,
  VStack,
  Flex,
  Box,
  Input,
} from "@chakra-ui/core";
import { Link, LinkButton } from "chakra-next-link";
import { NextPage } from "next";
import React, { useState } from "react";
import useSWR from "swr";
// import Input from "../../components/Input";

import { UserLayout } from "../../layouts/UserLayout";
import { api, useAppContext } from "../_app";

const ChatPage: NextPage = () => {
  const [userId, setUserId] = useState<number>();
  const { data, error, revalidate } = useSWR("/chats");

  const app = useAppContext();

  const onCreate = async () => {
    await api("/chats", {
      body: JSON.stringify({ otherId: userId }),
      method: "POST",
    });

    await revalidate();
  };

  console.log();

  return (
    <UserLayout>
      <Flex direction="column">
        <Flex justify="center" m="10px">
          <Heading color="red.700" mr="20px">
            {" "}
            Chats{" "}
          </Heading>

          <HStack spacing="2">
            <Input
              name="userId"
              type="number"
              value={userId}
              onChange={(e) => setUserId(parseInt(e.target.value))}
            />
            <Button onClick={onCreate}>Create</Button>
          </HStack>
        </Flex>

        {error ? (
          <Heading>Error...</Heading>
        ) : !data ? (
          <Spinner />
        ) : (
          <HStack spacing="3" mb="30px" mt="30px">
            <Heading size="lg" color="blue.800">
              Group message
            </Heading>
            {data.map((chat) => (
              <LinkButton
                bg="blue.100"
                p="4px 40px"
                borderRadius="5px"
                href={`/chat/direct/${chat.id}`}
              >
                {chat.id}. Box
              </LinkButton>
            ))}
          </HStack>
        )}

        <HStack spacing="4">
          <Heading size="lg" color="blue.800">
            Direct message
          </Heading>
          {[
            ...(app.data?.user?.groups || []),
            ...(app.data.user?.members || []).map((m) => m.groups),
          ].map((group) => (
            <LinkButton
              bg="blue.100"
              p="4px 40px"
              borderRadius="5px"
              //@ts-ignore
              href={`/chat/group/${group.id}`}
            >
              {(group as any).name}
            </LinkButton>
          ))}
        </HStack>
      </Flex>
    </UserLayout>
  );
};

export default ChatPage;
