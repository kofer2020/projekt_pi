import {
  Box,
  Button,
  Heading,
  HStack,
  Text,
  Spinner,
  UnorderedList,
  VStack,
  Flex,
} from "@chakra-ui/core";
import { Link } from "chakra-next-link";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useState } from "react";
import useSWR from "swr";
import Input from "../../../components/Input";
import { getId } from "../../../helpers/getId";

import { UserLayout } from "../../../layouts/UserLayout";
import { api } from "../../_app";

const ChatPage: NextPage = () => {
  const [message, setMessage] = useState<string>();

  const router = useRouter();
  const id = getId(router.query);
  const { data, error, revalidate } = useSWR(id ? `/chats/${id}` : null, {
    refreshInterval: 200,
  });

  if (id === null) {
    return <Heading>Error</Heading>;
  }

  const onSend = async () => {
    await api(`/chats/${id}`, {
      method: "POST",
      body: JSON.stringify({ content: message }),
    });
    await revalidate();
    setMessage("");
  };

  const users = data
    ? {
        [data.user1.id]: data.user1.username,
        [data.user2.id]: data.user2.username,
      }
    : {};

  return (
    <UserLayout>
      <Box height="80vh" overflowY="auto">
        <Flex justify="center">
          <Heading color="red.700"> Chat </Heading>
        </Flex>

        {error ? (
          <Heading>Error...</Heading>
        ) : !data ? (
          <Spinner />
        ) : (
          <VStack align="flex-start">
            <Flex width="30%" justify="space-between">
              <Heading size="md">
                User1: <i> {data.user1.username}</i>
              </Heading>
              <Heading size="md">
                User2: <i>{data.user2.username}</i>
              </Heading>
            </Flex>

            {data.messages.map((message) => (
              <Box ml="4" p="4" borderRadius="2" shadow="md">
                <Text>
                  <Text fontWeight="bold" as="span" mr="2">
                    {users[message.player_id]}:
                  </Text>
                  {message.content}
                </Text>
              </Box>
            ))}

            <HStack align="flex-end">
              <Input
                name="Message"
                onChange={(e) => setMessage(e.target.value)}
                value={message}
              />
              <Button onClick={onSend}>Send</Button>
            </HStack>
          </VStack>
        )}
      </Box>
    </UserLayout>
  );
};

export default ChatPage;
