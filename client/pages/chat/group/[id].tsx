import {
  Box,
  Button,
  Heading,
  HStack,
  Text,
  Spinner,
  UnorderedList,
  VStack,
} from "@chakra-ui/core";
import { Link } from "chakra-next-link";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useState } from "react";
import useSWR from "swr";
import Input from "../../../components/Input";
import { getId } from "../../../helpers/getId";

import { UserLayout } from "../../../layouts/UserLayout";
import { api } from "../../_app";

const ChatPage: NextPage = () => {
  const [message, setMessage] = useState<string>();

  const router = useRouter();
  const id = getId(router.query);
  const { data, error, revalidate } = useSWR(
    id ? `/groupMessages/${id}` : null,
    {
      refreshInterval: 200,
    }
  );

  if (id === null || error || data?.error) {
    return <Heading>{data?.error || "Error"}</Heading>;
  }

  const onSend = async () => {
    await api(`/groupMessages/${id}`, {
      method: "POST",
      body: JSON.stringify({ content: message }),
    });
    await revalidate();
    setMessage("");
  };

  const memberObj = (data
    ? [...data?.groupMembers?.members, data?.groupMembers.owner]
    : []
  ).reduce((prev, curr) => {
    const id = curr.id;

    if (id) {
      prev[id] = `[${id}]${curr.username}`;
    } else {
      prev[curr.users.id] = `[${curr.users.id}]${curr.users.username}`;
    }

    return prev;
  }, {});

  return (
    <UserLayout>
      <VStack align="flex-start">
        <Heading>Chats</Heading>

        {error ? (
          <Heading>Error...</Heading>
        ) : !data ? (
          <Spinner />
        ) : (
          <VStack align="flex-start">
            {Object.keys(memberObj).map((member, index) => (
              <Heading size="md">
                User{index}: {memberObj[member]}
              </Heading>
            ))}
            {data.messages.map((message) => (
              <Box p="4" borderRadius="2" shadow="md">
                <Text>
                  <Text fontWeight="bold" as="span" mr="2">
                    {memberObj[message.player_id]}:
                  </Text>
                  {message.content}
                </Text>
              </Box>
            ))}

            <HStack align="flex-end">
              <Input
                name="Message"
                onChange={(e) => setMessage(e.target.value)}
                value={message}
              />
              <Button onClick={onSend}>Send</Button>
            </HStack>
          </VStack>
        )}
      </VStack>
    </UserLayout>
  );
};

export default ChatPage;
