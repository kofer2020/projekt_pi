import React, { useEffect } from "react";
import { Box, Heading, VStack, Spinner, HStack } from "@chakra-ui/core";
import { NextPage } from "next";

import useSWR from "swr";
import { ListGroup } from "../components/ListGroup";
import { AddIcon, ArrowUpIcon } from "@chakra-ui/icons";
import { LinkButton } from "chakra-next-link";
import { UserLayout } from "../layouts/UserLayout";
import { useAppContext } from "./_app";

const Home: NextPage = () => {
  const { data, revalidate } = useSWR("/group");
  const { data: invitations } = useSWR("/invites");

  return (
    <UserLayout
      children={
        <Box>
          <HStack justify="space-between">
            <LinkButton
              leftIcon={<AddIcon />}
              colorScheme="orange"
              variant="solid"
              href="/group/new"
              m="20px 13px"
            >
              Create group
            </LinkButton>
            <Heading color="red.700">Active groups</Heading>
            {invitations ? (
              <LinkButton
                leftIcon={<ArrowUpIcon />}
                href="/profile"
                colorScheme="green"
                variant="solid"
              >
                Invitation: {invitations.length}
              </LinkButton>
            ) : (
              <Spinner />
            )}
          </HStack>

          <VStack>
            {data === undefined ? (
              <Spinner />
            ) : (
              data.map((group) => (
                <ListGroup group={group} refetch={revalidate} />
              ))
            )}
          </VStack>
        </Box>
      }
    />
  );
};

export default Home;
