import React from "react";
import {
  Box,
  Heading,
  Flex,
  Spinner,
  VStack,
  Link,
  Avatar,
  SimpleGrid,
} from "@chakra-ui/core";
import { NextPage } from "next";
import { UserLayout } from "../../../layouts/UserLayout";
import useSWR from "swr";
import { useRouter } from "next/router";
import { getId } from "../../../helpers/getId";

const UserShowProfile = ({ data, id, error, revalidate }) => {
  if (error) return <Heading>Error...</Heading>;
  if (!data) return <Spinner />;

  return (
    <SimpleGrid columns={2} mb="10">
      <VStack>
        <Heading color="red.800">groups</Heading>
        {data.groups.map((group) => (
          <Box padding="3" width="60%" bg="orange.100" borderRadius="5px">
            <Heading size="md" color="gray.800">
              {group.name}
            </Heading>
          </Box>
        ))}
        {data.groups.length === 0 && <Heading size="lg">No groups</Heading>}
      </VStack>
      <VStack spacing="3">
        <Heading color="red.800">games</Heading>
        {data.plays.map(({ game }) => (
          <Box padding="3" width="60%" bg="blue.100" borderRadius="5px">
            <Link href={`/games/${id}`}>
              <Heading size="md" color="gray.800">
                {game.name}
              </Heading>
            </Link>
          </Box>
        ))}
      </VStack>
    </SimpleGrid>
  );
};

const UserPage: NextPage = () => {
  const router = useRouter();
  const id = getId(router.query);
  const { data, error, revalidate } = useSWR(id && `/admin/users/${id}`);

  return (
    <UserLayout>
      {data ? (
        <Box>
          <Flex justify="center" mb="40px">
            <Avatar src={data.photourl} name={data.username} mr="1%" />
            <Heading color="red.700"> {data.username} </Heading>
          </Flex>
          <UserShowProfile
            data={data}
            error={error}
            revalidate={revalidate}
            id={id}
          />
        </Box>
      ) : (
        <Spinner />
      )}
    </UserLayout>
  );
};

export default UserPage;
