import React from "react";
import { NextPage } from "next";
import useSWR from "swr";
import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  HStack,
  Select,
  Spinner,
  VStack,
} from "@chakra-ui/core";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import { LinkButton } from "chakra-next-link";

import { AdminLayout } from "../../../../layouts/AdminLayout";
import { getId } from "../../../../helpers/getId";
import Input from "../../../../components/Input";
import { Role, roles } from "../index";
import { api } from "../../../_app";

interface UserEditForm {
  username: string;
  photourl: string;
  is_active: boolean;
  user_role: Role;
}

const AdminEditUser = ({ data, error, revalidate }) => {
  if (error) return <Heading>Error...</Heading>;
  if (!data) return <Spinner />;

  const router = useRouter();

  const { register, handleSubmit, formState } = useForm<UserEditForm>({
    defaultValues: { ...data },
  });

  const onSubmit = async (values: UserEditForm) => {
    const res = await api(`/admin/users/${data.id}`, {
      method: "PATCH",
      body: JSON.stringify(values),
    });

    await revalidate();
    await router.push(`/admin/users/${data.id}`);
  };

  return (
    <VStack
      align="flex-end"
      spacing={4}
      as="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <Flex w="100%" justify="space-between">
        <HStack spacing={4}>
          <Heading>Edit user: {data.username}</Heading>
        </HStack>
        <LinkButton href={`/admin/users/${data.id}`}>Back to user</LinkButton>
      </Flex>
      <Input name="username" ref={register()} />
      <Input name="photourl" ref={register()} />

      <FormControl>
        <FormLabel htmlFor="user_role">User role</FormLabel>
        <Select name="user_role" ref={register()}>
          {roles.map((role) => (
            <option value={role} key={role}>
              {role}
            </option>
          ))}
        </Select>
      </FormControl>

      <Button
        type="submit"
        colorScheme="blue"
        isLoading={formState.isSubmitting}
        isDisabled={formState.isSubmitting}
      >
        Update
      </Button>
    </VStack>
  );
};

const AdminEditUserPage: NextPage = () => {
  const router = useRouter();
  const id = getId(router.query);
  const { data, error, revalidate } = useSWR(
    id === null ? null : `/admin/users/${id}`
  );

  return (
    <AdminLayout>
      <AdminEditUser data={data} error={error} revalidate={revalidate} />
    </AdminLayout>
  );
};

export default AdminEditUserPage;
