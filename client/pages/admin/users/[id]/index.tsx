import React from "react";
import { NextPage } from "next";
import useSWR from "swr";
import {
  Avatar,
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  Spinner,
  VStack,
} from "@chakra-ui/core";
import { useRouter } from "next/router";
import { LinkButton } from "chakra-next-link";

import { AdminLayout } from "../../../../layouts/AdminLayout";
import { getId } from "../../../../helpers/getId";
import { Table } from "../../../../components/Table";

const AdminShowUser = ({ data, error }) => {
  if (error) return <Heading>Error...</Heading>;
  if (!data) return <Spinner />;

  return (
    <VStack>
      <Flex w="100%" justify="space-between">
        <HStack spacing={4}>
          <Avatar src={data.photourl} name={data.username} />
          <Heading>{data.username}</Heading>
        </HStack>
        <LinkButton href={`/admin/users/${data.id}/edit`} colorScheme="green">
          Edit
        </LinkButton>
      </Flex>
      <Table>
        <thead>
          <tr>
            <th>key</th>
            <th>value</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(data).map((key) => (
            <tr>
              <td>{key}</td>
              <td>{JSON.stringify(data[key])}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </VStack>
  );
};

const AdminShowUserPage: NextPage = () => {
  const router = useRouter();
  const id = getId(router.query);
  const { data, error } = useSWR(`/admin/users/${id}`);

  return (
    <AdminLayout>
      <AdminShowUser data={data} error={error} />
    </AdminLayout>
  );
};

export default AdminShowUserPage;
