import React, { useState } from "react";
import { NextPage } from "next";
import useSWR from "swr";
import {
  Avatar,
  Button,
  Heading,
  HStack,
  Select,
  Spinner,
  useBoolean,
  VStack,
} from "@chakra-ui/core";
import { Link, LinkButton } from "chakra-next-link";

import { Table } from "../../../components/Table";
import { AdminLayout } from "../../../layouts/AdminLayout";
import { api } from "../../_app";

export const roles = ["Administrator", "Moderator", "Renter", "User"] as const;
export type Role = typeof roles[number];

interface ChangeRoleProps {
  userId: number;
  user_role: Role;
  refetch: () => Promise<any>;
}
export const ChangeRole: React.FC<ChangeRoleProps> = ({
  userId,
  user_role,
  refetch,
}) => {
  const [showSelect, { on, off }] = useBoolean(false);
  const [role, setRole] = useState<Role>(user_role);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const save = async () => {
    setIsLoading(true);
    const res = await api(`/admin/users/${userId}`, {
      method: "PATCH",
      body: JSON.stringify({ user_role: role }),
    });

    setIsLoading(false);

    if (res.ok) {
      await refetch();
      off();
    }
  };

  return showSelect ? (
    <HStack>
      <Select value={role} onChange={(e) => setRole(e.target.value as Role)}>
        {roles.map((role) => (
          <option value={role}>{role}</option>
        ))}
      </Select>
      <Button onClick={save} isDisabled={isLoading} isLoading={isLoading}>
        Save
      </Button>
    </HStack>
  ) : (
    <Button onClick={on}>Change role</Button>
  );
};

const ConfirmUser = ({ userId, revalidate }) => {
  const [isLoading, { off, on }] = useBoolean(false);
  const confirmUser = async () => {
    on();
    const res = await api(`/admin/users/${userId}/accept`, {
      method: "PATCH",
    });

    off();

    if (res.ok) {
      await revalidate();
    }
  };

  return (
    <Button
      colorScheme="orange"
      onClick={confirmUser}
      isLoading={isLoading}
      isDisabled={isLoading}
    >
      Confirm
    </Button>
  );
};

const UserRow = ({ user, revalidate }) => {
  const onDelete = async () => {
    const res = confirm("Are you sure?");
    if (res) {
      await api(`/admin/users/${user.id}`, { method: "DELETE" }, true);
      await revalidate();
    }
  };

  return (
    <tr>
      <td>{user.id}</td>
      <td>
        <Avatar name={user.username} src={user.photourl} />
      </td>
      <td>{user.email}</td>
      <td>
        <Link href={`/admin/users/${user.id}`}>{user.username}</Link>
      </td>
      <td>{user.user_role}</td>
      <td>
        <HStack>
          <ChangeRole
            refetch={revalidate}
            userId={user.id}
            user_role={user.user_role}
          />
          {!user.is_accepted && (
            <ConfirmUser userId={user.id} revalidate={revalidate} />
          )}
          <LinkButton href={`/admin/users/${user.id}/edit`} colorScheme="green">
            Edit
          </LinkButton>
          <Button colorScheme="red" onClick={onDelete}>
            Delete
          </Button>
        </HStack>
      </td>
    </tr>
  );
};

const AdminUsers = ({ data, error, revalidate }) => {
  if (error) return <Heading>Error...</Heading>;
  if (!data) return <Spinner />;

  return (
    <VStack align="flex-start">
      <Heading>Users</Heading>
      <Table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Email</th>
            <th>username</th>
            <th>role</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          {data.map((user) => (
            <UserRow key={user.id} user={user} revalidate={revalidate} />
          ))}
        </tbody>
      </Table>
    </VStack>
  );
};

const AdminUsersPage: NextPage = () => {
  const { data, error, revalidate } = useSWR("/admin/users");

  return (
    <AdminLayout>
      <AdminUsers data={data} error={error} revalidate={revalidate} />
    </AdminLayout>
  );
};

export default AdminUsersPage;
