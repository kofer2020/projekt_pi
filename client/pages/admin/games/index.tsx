import React from "react";
import { NextPage } from "next";
import useSWR from "swr";
import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  Spinner,
  VStack,
} from "@chakra-ui/core";

import { Table } from "../../../components/Table";
import { AdminLayout } from "../../../layouts/AdminLayout";
import { Link, LinkButton } from "chakra-next-link";
import { approveGame, deleteGame } from "../../../lib/api/admin/games";

const AdminGamesTable = ({ games, revalidate, title }) => {
  return (
    <VStack w="100%" align="flex-start">
      <Heading size="lg">{title}</Heading>
      <Table>
        <thead>
          <tr>
            <Box as="th" w="5%">
              ID
            </Box>
            <Box as="th" w="20%">
              Name
            </Box>
            <Box as="th" w="30%">
              Description
            </Box>
            <Box as="th" w="20%">
              Category
            </Box>
            <Box as="th" w="25%">
              Options
            </Box>
          </tr>
        </thead>
        <tbody>
          {games.map((game) => (
            <tr>
              <td>{game.id}</td>
              <td>
                <Link href={`/admin/games/${game.id}`}>{game.name}</Link>
              </td>
              <td>{game.description.slice(0, 100)}</td>
              <td>{game.category.name}</td>
              <td>
                <HStack>
                  <LinkButton
                    href={`/admin/games/${game.id}/edit`}
                    colorScheme="green"
                  >
                    Edit
                  </LinkButton>
                  <Button
                    onClick={deleteGame(game.id, revalidate)}
                    colorScheme="red"
                  >
                    Delete
                  </Button>
                  {game.is_suggestion && (
                    <Button
                      onClick={approveGame(game.id, revalidate)}
                      colorScheme="orange"
                    >
                      Approve
                    </Button>
                  )}
                </HStack>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </VStack>
  );
};

const AdminGames = ({ data, error, revalidate }) => {
  if (error) return <Heading>Error...</Heading>;
  if (!data) return <Spinner />;

  const { pending, approved } = data.reduce(
    (prev, game) => {
      if (game.is_suggestion) {
        prev.pending.push(game);
      } else {
        prev.approved.push(game);
      }

      return prev;
    },
    { pending: [], approved: [] }
  );

  return (
    <VStack align="flex-start" spacing={8}>
      <Flex justify="space-between" w="100%">
        <Heading>Games</Heading>
        <LinkButton colorScheme="blue" href="/games/new">
          New game
        </LinkButton>
      </Flex>

      <AdminGamesTable
        title="Pending"
        revalidate={revalidate}
        games={pending}
      />
      <AdminGamesTable
        title="Approved"
        revalidate={revalidate}
        games={approved}
      />
    </VStack>
  );
};

const AdminGamesPage: NextPage = () => {
  const { data, error, revalidate } = useSWR("/admin/games");

  return (
    <AdminLayout>
      <AdminGames data={data} error={error} revalidate={revalidate} />
    </AdminLayout>
  );
};

export default AdminGamesPage;
