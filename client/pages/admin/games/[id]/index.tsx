import React from "react";
import useSWR from "swr";
import { NextPage } from "next";
import { useRouter } from "next/router";
import {
  Button,
  Flex,
  Heading,
  HStack,
  Spinner,
  VStack,
} from "@chakra-ui/core";

import { AdminLayout } from "../../../../layouts/AdminLayout";
import { getId } from "../../../../helpers/getId";
import { Table } from "../../../../components/Table";
import { LinkButton } from "chakra-next-link";
import { approveGame, deleteGame } from "../../../../lib/api/admin/games";

const AdminShowGame = ({ data, error, revalidate }) => {
  const router = useRouter();

  if (error) return <Heading>Error...</Heading>;
  if (!data) return <Spinner />;

  return (
    <VStack>
      <Flex w="100%" justify="space-between">
        <Heading>{data.name}</Heading>
        <HStack spacing={4}>
          <LinkButton href={`/admin/games/${data.id}/edit`} colorScheme="green">
            Edit
          </LinkButton>
          <Button
            onClick={deleteGame(data.id, async () => {
              await revalidate();
              await router.push("/admin/games");
            })}
            colorScheme="red"
          >
            Delete
          </Button>
          {data.is_suggestion && (
            <Button
              onClick={approveGame(data.id, revalidate)}
              colorScheme="orange"
            >
              Approve
            </Button>
          )}
        </HStack>
      </Flex>
      <Table>
        <thead>
          <tr>
            <th>key</th>
            <th>value</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(data).map((key) => (
            <tr>
              <td>{key}</td>
              <td>
                {typeof data[key] === "object"
                  ? JSON.stringify(data[key])
                  : typeof data[key] === "boolean"
                  ? data[key]
                    ? "true"
                    : "false"
                  : data[key]}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </VStack>
  );
};

const AdminShowGamePage: NextPage = () => {
  const router = useRouter();
  const id = getId(router.query);
  const { data, error, revalidate } = useSWR(`/game/${id}`);

  return (
    <AdminLayout>
      <AdminShowGame data={data} error={error} revalidate={revalidate} />
    </AdminLayout>
  );
};

export default AdminShowGamePage;
