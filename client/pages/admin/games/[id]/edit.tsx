import React from "react";
import { NextPage } from "next";
import useSWR from "swr";
import {
  Avatar,
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  HStack,
  Select,
  Spinner,
  VStack,
} from "@chakra-ui/core";
import { useRouter } from "next/router";
import { Link, LinkButton } from "chakra-next-link";
import { Controller, useForm } from "react-hook-form";

import { AdminLayout } from "../../../../layouts/AdminLayout";
import { getId } from "../../../../helpers/getId";
import Input from "../../../../components/Input";
import { Checkbox } from "../../../../components/Checkbox";
import { api } from "../../../_app";

export interface GameEditForm {
  name: string;
  photo_url: string;
  description: string;
  max_players: string;
  duration: string;
  link: string;
  category_id: string;
  no_of_copies: string;
  is_suggestion: boolean;
}

interface GameEditFormApi {
  name: string;
  photo_url: string;
  description: string;
  max_players: number;
  duration: number;
  link: string;
  category_id: number;
  no_of_copies: number;
  is_suggestion: boolean;
}

const AdminEditGame = ({ data, error, revalidate }) => {
  if (error) return <Heading>Error...</Heading>;
  if (!data) return <Spinner />;

  const { data: categories, error: categoriesError } = useSWR("/category");

  const router = useRouter();
  const { register, handleSubmit, formState, control } = useForm<GameEditForm>({
    defaultValues: { ...data },
  });

  console.log(data);

  const onSubmit = async (values: GameEditForm) => {
    console.log(values);

    const body: GameEditFormApi = {
      ...values,
      category_id: parseInt(values.category_id),
      duration: parseInt(values.duration),
      max_players: parseInt(values.max_players),
      no_of_copies: parseInt(values.max_players),
    };

    const res = await api(`/game/${data.id}`, {
      method: "PATCH",
      body: JSON.stringify(body),
    });

    await revalidate();
    await router.push(`/admin/games/${data.id}`);
  };

  return (
    <VStack
      align="flex-end"
      spacing={4}
      as="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <Flex w="100%" justify="space-between">
        <HStack spacing={4}>
          <Heading>Edit game: {data.username}</Heading>
        </HStack>
        <LinkButton href={`/admin/games/${data.id}`}>Back to game</LinkButton>
      </Flex>

      <Input name="name" ref={register()} />
      <Input name="photo_url" ref={register()} />
      <Input name="description" ref={register()} />
      <Input
        name="max_players"
        label="Max players"
        ref={register}
        type="number"
      />
      <Input name="duration" ref={register} type="number" />
      <Input name="link" ref={register()} />
      <Input
        name="no_of_copies"
        label="Number of copies"
        ref={register}
        type="number"
      />

      <FormControl>
        <FormLabel>Category</FormLabel>
        {categories === undefined ? (
          <Spinner />
        ) : categoriesError ? (
          <Heading>Error loading categories</Heading>
        ) : (
          <Select name="category_id" ref={register()} isRequired>
            {categories.map((category) => (
              <option value={category.id}>{category.name}</option>
            ))}
          </Select>
        )}
      </FormControl>

      <Box w="100%">
        <Checkbox
          control={control}
          name="is_suggestion"
          label="Is suggestion"
          defaultValue={data.is_suggestion}
        />
      </Box>

      <Button
        type="submit"
        colorScheme="blue"
        isLoading={formState.isSubmitting}
        isDisabled={formState.isSubmitting}
      >
        Update
      </Button>
    </VStack>
  );
};

const AdminEditGamePage: NextPage = () => {
  const router = useRouter();
  const id = getId(router.query);
  const { data, error, revalidate } = useSWR(
    id === null ? null : `/game/${id}`
  );

  return (
    <AdminLayout>
      <AdminEditGame data={data} error={error} revalidate={revalidate} />
    </AdminLayout>
  );
};

export default AdminEditGamePage;
