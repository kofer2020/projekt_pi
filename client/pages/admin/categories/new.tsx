import React from "react";
import { NextPage } from "next";
import useSWR from "swr";
import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  HStack,
  Select,
  Spinner,
  VStack,
} from "@chakra-ui/core";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import { LinkButton } from "chakra-next-link";

import { AdminLayout } from "../../../layouts/AdminLayout";
import Input from "../../../components/Input";
import { api } from "../../_app";

interface CategoryEditForm {
  name: string;
}

const AdminNewCategory = () => {
  const router = useRouter();

  const { register, handleSubmit, formState } = useForm<CategoryEditForm>();

  const onSubmit = async (values: CategoryEditForm) => {
    const res = await api(`/category`, {
      method: "POST",
      body: JSON.stringify({ name: values.name }),
    });

    await router.push(`/admin/categories`);
  };

  return (
    <VStack
      align="flex-end"
      spacing={4}
      as="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <Flex w="100%" justify="space-between">
        <HStack spacing={4}>
          <Heading>New category</Heading>
        </HStack>
        <LinkButton href={`/admin/categories`}>Back to categories</LinkButton>
      </Flex>
      <Input name="name" ref={register()} />

      <Button
        type="submit"
        colorScheme="blue"
        isLoading={formState.isSubmitting}
        isDisabled={formState.isSubmitting}
      >
        Create
      </Button>
    </VStack>
  );
};

const AdminNewCategoryPage: NextPage = () => {
  const router = useRouter();

  return (
    <AdminLayout>
      <AdminNewCategory />
    </AdminLayout>
  );
};

export default AdminNewCategoryPage;
