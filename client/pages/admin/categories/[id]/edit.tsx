import React from "react";
import { NextPage } from "next";
import useSWR from "swr";
import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  HStack,
  Select,
  Spinner,
  VStack,
} from "@chakra-ui/core";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import { LinkButton } from "chakra-next-link";

import { AdminLayout } from "../../../../layouts/AdminLayout";
import { getId } from "../../../../helpers/getId";
import Input from "../../../../components/Input";
import { api } from "../../../_app";

interface CategoryEditForm {
  name: string;
}

const AdminEditCategory = ({ data, error, revalidate }) => {
  if (error) return <Heading>Error...</Heading>;
  if (!data) return <Spinner />;

  const router = useRouter();

  const { register, handleSubmit, formState } = useForm<CategoryEditForm>({
    defaultValues: { ...data },
  });

  const onSubmit = async (values: CategoryEditForm) => {
    const res = await api(`/category/${data.id}`, {
      method: "PATCH",
      body: JSON.stringify({ name: values.name }),
    });

    await revalidate();
    await router.push(`/admin/categories`);
  };

  return (
    <VStack
      align="flex-end"
      spacing={4}
      as="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <Flex w="100%" justify="space-between">
        <HStack spacing={4}>
          <Heading>Edit category: {data.name}</Heading>
        </HStack>
        <LinkButton href={`/admin/categories`}>Back to categories</LinkButton>
      </Flex>
      <Input name="name" ref={register()} />

      <Button
        type="submit"
        colorScheme="blue"
        isLoading={formState.isSubmitting}
        isDisabled={formState.isSubmitting}
      >
        Update
      </Button>
    </VStack>
  );
};

const AdminEditCategoryPage: NextPage = () => {
  const router = useRouter();
  const id = getId(router.query);
  const { data, error, revalidate } = useSWR(
    id === null ? null : `/category/${id}`
  );

  return (
    <AdminLayout>
      <AdminEditCategory data={data} error={error} revalidate={revalidate} />
    </AdminLayout>
  );
};

export default AdminEditCategoryPage;
