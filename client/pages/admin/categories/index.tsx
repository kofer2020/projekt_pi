import React from "react";
import { NextPage } from "next";
import useSWR from "swr";
import { Flex, Heading, HStack, Spinner, VStack } from "@chakra-ui/core";
import { LinkButton } from "chakra-next-link";

import { Table } from "../../../components/Table";
import { AdminLayout } from "../../../layouts/AdminLayout";

const AdminCategories = ({ data, error, revalidate }) => {
  if (error) return <Heading>Error...</Heading>;
  if (!data) return <Spinner />;

  return (
    <VStack align="flex-start">
      <Flex justifyContent="space-between" w="100%">
        <Heading>Categories</Heading>
        <LinkButton href="/admin/categories/new">New</LinkButton>
      </Flex>
      <Table>
        <thead>
          <tr>
            <th>ID</th>
            <th>name</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          {data.map((category) => (
            <tr>
              <td>{category.id}</td>
              <td>{category.name}</td>
              <td>
                <HStack>
                  <LinkButton
                    href={`/admin/categories/${category.id}/edit`}
                    colorScheme="green"
                  >
                    Edit
                  </LinkButton>
                </HStack>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </VStack>
  );
};

const AdminCategoriesPage: NextPage = () => {
  const { data, error, revalidate } = useSWR("/category");

  return (
    <AdminLayout>
      <AdminCategories data={data} error={error} revalidate={revalidate} />
    </AdminLayout>
  );
};

export default AdminCategoriesPage;
