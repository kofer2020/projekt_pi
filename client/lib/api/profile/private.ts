import { api } from "../../../pages/_app";

export interface PlayForm {
  game_id: number;
  offer_to_rent: boolean;
  price: number;
  count: number;
}

export const createPlay = async (
  values: PlayForm,
  revalidate: () => Promise<any>
) => {
  const body = {
    gameId: values.game_id,
    offerToRent: values.offer_to_rent,
    price: values.price,
    count: values.count
  };

  await api(`/plays`, {
    method: "POST",
    body: JSON.stringify(body)
  });

  await revalidate();
};

export const removePlay = async (
  gameId: number,
  revalidate: () => Promise<any>
) => {
  const res = await api(`/plays`, {
    method: "DELETE",
    body: JSON.stringify({ gameId }),
  });

  await revalidate();
};

export const updatePlay = async (
  gameId: number,
  rentable: boolean,
  price: number,
  count: number,
  revalidate: () => Promise<any>
) => {
  const res = await api(`/plays`, {
    method: "PATCH",
    body: JSON.stringify({ gameId, offerToRent: rentable, price, count }),
  });

  await revalidate();
};
