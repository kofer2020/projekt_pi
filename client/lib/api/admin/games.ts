import { api } from "../../../pages/_app";

export const approveGame = (
  id: number,
  revalidate?: () => Promise<any>
) => async () => {
  const res = await api(`/game/approve/${id}`, {
    method: "POST",
  });

  await revalidate();
};

export const deleteGame = (
  id: number,
  revalidate?: () => Promise<any>
) => async () => {
  const res = await api(`/game/${id}`, {
    method: "DELETE",
  });

  await revalidate();
};
