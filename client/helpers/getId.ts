import { ParsedUrlQuery } from 'querystring';

export const getId = (query: ParsedUrlQuery, key: string = 'id'): null | number => {
  if (query[key]) {
    if (Array.isArray(query[key])) {
      return parseInt(query[key][0]);
    }

    return parseInt(query[key] as string);
  }

  return null;
};