import { useEffect } from "react";
import { useRouter } from "next/router";

export const useGoHomeIfLoggedIn = () => {
  const router = useRouter();

  useEffect(() => {
    if (localStorage && localStorage.getItem("token")) {
      router.push("/home");
    }
  }, []);
};
