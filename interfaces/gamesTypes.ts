import { category } from "@prisma/client";

export interface Game {
  id?: number;
  name: string;
  description: string;
  photo_url?: string;
  max_players: number;
  duration: number; //Minutes
  link: string; //third-party link
  no_of_copies: number;
  category: category;
  is_suggestion: boolean;
}

export interface Suggestion extends Game {}

export interface Category {
  id?: number;
  name: string;
}
