import { ad, game, groups, invitation_states, users } from "@prisma/client";
import { Game } from "./gamesTypes";

export interface Group{
    game: game;
    id?: number
    capacity: number,
    name: string,
    owner_id: number,
    description?: string,
}

/**
 * Invitations to join group
 */
export interface Invitation{
   group_id: number,
   player_id: number,
   text?: string,
   state: invitation_states,
   sent_at: Date
}

export interface Request{
    player: users,
    adv: ad,
}

/**
 * Request resolution type
 */
export interface InviteResolution{
    player_id: number,
    group_id: number,
    accepted: boolean,
}

/**
 * Type for resolving join requests
 */
export interface RequestResolution{
    player_id: number,
    ad_id: number,
    accepted: boolean,
}

/**
 * Location used to indicate where game will be played.
 */
export interface Location{
    longitude: number,
    latitude: number,
}

/**
 * Ad which group owner create
 */
export interface Ad{
    id?: number,
    description: string,
    location: Location,
    group: groups
}