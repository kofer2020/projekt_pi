export interface LoginType {
  email: string;
  password: string;
}

export interface RegistrationType {
  username: string;
  email: string;
  password: string;
  photourl?: string;
  isCompany: boolean;
  companyId?: number;
  companyName?: string;
  companyAddress?: string;
  companyTelephone?: string;
  companyWebsite?: string;
}

export interface RegistrationError {
  errorMsg: string;
}
