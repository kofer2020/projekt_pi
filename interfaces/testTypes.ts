export interface TitleType {
    title: string,
}

export interface User{
    username: string,
    role: string,
    dateOfBirth: string,
    averageGrade: number,
}