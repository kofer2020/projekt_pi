export interface UserProfile{
    username: string,
    rating: Number,
    email: string,
    photourl?: string,
}

export interface RenterProfile{
    username: string,
    email: Number,
    photourl?: string,
}   

export interface ModeratorProfile extends RenterProfile{
}

export interface AdministratorProfile extends RenterProfile{
}

export interface UpdateProfile{
    username?: string,
    email?: string,
    photourl?: string,
    password: string
}