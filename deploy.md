Start postgres CLI: heroku pg:psql -a game-ex
    Show DB tables: \dt
    Exit postgres CLI: exit
Build database from .sql file on heroku: cat filename.sql | heroku pg:psql -a game-ex
Connect prisma client to fresh DB instance: npx prisma introspect && npx prisma generate
